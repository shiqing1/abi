#!/bin/bash 

mode=$1
modeldate=$2
testsetdate=$3

if [ $# -ne 3 ];then
	echo "Usage:./test_detecton.sh  mode[group/single] modeldate testsetdate"
	exit
fi

mkdir -vp /home/haiyan_zeng/data/TestDataResult/${mode}/${testsetdate}_test/${modeldate}_images/
python inference_images_${mode}.py\
	--saved-model-path /home/haiyan_zeng/code/ABI_TT_model/${modeldate}_${mode}_models_inference_graph/saved_model\
	--label-map-path /home/haiyan_zeng/code/ABI_TT_model/train_config/${mode}_detection/labelmap_${modeldate}.pbtxt\
	--image-dir /home/haiyan_zeng/data/TestData/${mode}/${testsetdate}_test/images\
	--destination-predict-image-dir /home/haiyan_zeng/data/TestDataResult/${mode}/${testsetdate}_test/${modeldate}_images\
	--threshold 0.6\
	--dest-csv-path /home/haiyan_zeng/data/TestDataResult/${mode}/${testsetdate}_test/${modeldate}_model_${testsetdate}_test_result.csv\

cd /home/haiyan_zeng/code/thor/Metric_Services/eval_service/abi_process
python eval_multiclass_maskrcnn.py\
	--dir /home/haiyan_zeng/data/TestData/${mode}/${testsetdate}_test/images/\
	--gtcsv /home/haiyan_zeng/data/TestData/${mode}/${testsetdate}_test/${testsetdate}_${mode}_test_gt.csv\
	--predcsv  /home/haiyan_zeng/data/TestDataResult/${mode}/${testsetdate}_test/${modeldate}_model_${testsetdate}_test_result.csv\
	--outdir  /home/haiyan_zeng/results/${testsetdate}_${mode}_${modeldate}_model

cd /home/haiyan_zeng/results
python recall_precision_report.py  -f /home/haiyan_zeng/results/${testsetdate}_${mode}_${modeldate}_model/PerSkuPrecisionRecall.csv
