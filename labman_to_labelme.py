#!/usr/bin/env python
#-*- coding:utf-8 -*-
"""
transform labman csv to labelme json(which is an opened software on anaconda)
@Author:Zeng Haiyan
@Date:20200907
"""

import pandas as pd
import argparse
from pathlib import Path
from json import dumps
import os
from image_tools import download_image_labman,str_tools
from base64 import b64encode
import cv2
import numpy as np

parser = argparse.ArgumentParser()
parser.add_argument('-i','--input-csv-dir',type=Path,default=r'/home/haiyan_zeng/demo')
parser.add_argument('-o','--output-json-dir',type=str,default=r'/home/haiyan_zeng/demo/')
parser.add_argument('-s','--destination-image-dir',type=str,default=r'/home/haiyan_zeng/demo/imgs')

def image_to_bs4string(img_path):
    """
    image to bs4string
    :param img_path:
    :return:
    """
    with open(img_path, 'rb') as jpg_file:
        byte_content = jpg_file.read()
        base64_bytes = b64encode(byte_content)
        base64_string = base64_bytes.decode('utf-8')
    return base64_string

def fetch_image_label_point_json(polygon,label,width,height):
    """
    polygon to labelme shapes
    :param polygon:
    :param label:
    :param width:
    :param height:
    :return:
    """
    label_point_json = {}
    label_point_json['label'] = str(label)
    label_point_json['group_id'] = None
    label_point_json['shape_type'] = "polygon"
    label_point_json['flags'] = {}
    label_point_json['points'] = str_tools.get_str_polygon_to_list(polygon,width,height)

    return label_point_json

def infor_to_json(img_shapes,img_local_path,img_data,img_height,img_width,output_json_dir):
    """
    csv info to labelme json
    :param img_shapes:
    :param img_local_path:
    :param img_data:
    :param img_height:
    :param img_width:
    :param output_json_dir:
    :return:
    """
    #conduct json
    labelme_json = {}
    labelme_json['version'] = "4.5.6"
    labelme_json['flags'] = {}
    labelme_json['shapes'] = img_shapes
    labelme_json['imagePath'] = img_local_path
    labelme_json['imageData'] = img_data
    labelme_json['imageHeight'] = img_height
    labelme_json['imageWidth'] = img_width

    # save json file
    json_data = dumps(labelme_json, indent=2)
    img_name = img_local_path[img_local_path.rindex('\\')+1:img_local_path.rindex('.jpg')]
    write_json_path = os.path.join(output_json_dir,img_name+'.json')
    with open(write_json_path,'w') as json_file:
        json_file.write(json_data)

def labman_to_labelme(input_csv_dir,output_json_dir,destination_image_dir):
    """
    transfer labman csv to labelme json
    :param args:
    :return:
    """
    # read batch csv file from directory
    df = pd.concat([pd.read_csv(p) for p in _get_file_path(Path(input_csv_dir))])

    #one image will generate a json file
    img_group = df.groupby('ImageUrl')
    for img_url, group_item in img_group:
        #-------------------------------------judge image and json exist-------------------------------------------
        if '.jpg' in img_url:
            img_name = img_url[img_url.rindex('/') + 1:img_url.rindex('.')]
        else:
            img_name = img_url[img_url.rindex('/') + 1:len(img_url)]
        if not os.path.exists(destination_image_dir):
            os.mkdir(destination_image_dir)

        image_file_path = os.path.join(destination_image_dir, img_name + '.jpg')
        write_json_path = os.path.join(output_json_dir, img_name + '.json')

        if os.path.exists(image_file_path) and os.path.exists(write_json_path):
            print('------It is downloaded--------')
            print(img_name)
        else:
            img_local_path = download_image_labman.download_image(img_url,destination_image_dir)
            img_data = image_to_bs4string(img_local_path)
            (img_height,img_width,channel) = np.shape(cv2.imread(img_local_path))

            #get all point of one image
            img_shapes = []
            for (polygon,label) in zip(group_item['Polygon'].values.tolist(),group_item['ProductId'].values.tolist()):
                img_shapes.append(fetch_image_label_point_json(polygon,label,img_width,img_height))

            infor_to_json(img_shapes,img_local_path,img_data,img_height,img_width,output_json_dir)

def _get_file_path(dir):
    """
    get file path list of dir
    :param dir:
    :return:
    """
    if dir.is_dir():
        return sorted(dir.rglob("*.csv"))
    return [dir]

def main(args):
    labman_to_labelme(args.input_csv_dir,args.output_json_dir,args.destination_image_dir)

if __name__ == '__main__':
    main(parser.parse_args())
