'''
date:20201209
author:shiqing
'''

import os 
import pandas as pd
import shutil
import argparse

parser = argparse.ArgumentParser()
parser.add_argument('-s','--src',type=str,default='/home/haiyan_zeng/data/TrainData/images/group_detection/20201225/total')
parser.add_argument('-i','--input_file',type=str,default='/home/haiyan_zeng/data/TrainData/csv/group_detection/20201225_merge_aug.csv')
parser.add_argument('-o','--output_file',type=str,default='NumsOfEachClass.csv')

def train_test_split(src,input_file,output_file):

    df = pd.read_csv(input_file)
    groups = df.groupby("ProductId")

    print(len(groups))

    out = pd.DataFrame(columns=('ProductId','Nums'))
    train_path = os.path.join(src[:-6],'train')
    test_path = os.path.join(src[:-6],'test')

    for g in groups:
        id = g[0]
        data = g[1]
        out.loc[out.shape[0]]={'ProductId':id,'Nums':data.shape[0]}
        urls = data['ImageUrl'].to_list()
        if data.shape[0]>10:#样本多,9:1
            nums = int(data.shape[0]*0.9)#9:1分割训练集和测试集
            left = urls[:nums]
            right = urls[nums:]
        elif data.shape[0]>1:#样本少，留一
            nums = data.shape[0]-1
            left = urls[:nums]
            right = urls[nums:]
        else:#只有一张，进训练和测试
            left = urls
            right = urls


        for url in left:
            name = url.split("/")[-1]
            if '.jpg' not in name:
                jpgname = name+'.jpg'
                jsonname = name+'.json'
            if os.path.exists(os.path.join(src,jpgname)):
                shutil.copyfile(os.path.join(src,jpgname),os.path.join(train_path,jpgname))
                shutil.copyfile(os.path.join(src,jsonname),os.path.join(train_path,jsonname))

        for url in right:
            name = url.split("/")[-1]
            if '.jpg' not in name:
                jpgname = name+'.jpg'
                jsonname = name+'.json'
            if os.path.exists(os.path.join(src,jpgname)):
                shutil.copyfile(os.path.join(src,jpgname),os.path.join(test_path,jpgname))
                shutil.copyfile(os.path.join(src,jsonname),os.path.join(test_path,jsonname))
             
    out.sort_values(by=['Nums']) 
    out.to_csv(output_file,index=False)

if __name__ == "__main__":
    args = parser.parse_args()
    train_test_split(args.src,args.input_file,args.output_file)
