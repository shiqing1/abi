# abi maskrcnn train and eval

### 1. 
```
cd ~
git clone https://github.com/tensorflow/models
```
compile **object_detection** module and download pretrained model files.[Reference](https://github.com/tensorflow/models/blob/master/research/object_detection/g3doc/tf2.md)   
we use `mask_rcnn_inception_resnet_v2_1024x1024_coco17_gpu-8` as our pretrained model.



### 2. data augmentation
```
cd chimpanzee
./augment_and_merge.sh  mode[group/single] time 
```
### 3. train
```
./data_prepare_and_train.sh mode[group/single] time
```

### 4. eval
```
./test_detecton.sh  mode[group/single] modeldate testsetdate
```