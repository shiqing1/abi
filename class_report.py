import os                                                                                                                                                                                    
import  pandas as pd
import argparse

parser = argparse.ArgumentParser()
parser.add_argument('-f','--folder',type=str,required=True)
parser.add_argument('-o','--output',type=str,required=True)
parser.add_argument('-m','--mapfile',type=str,required=True)
args = parser.parse_args()
skuIdName = pd.read_csv(args.mapfile,low_memory=False)[["ProductId",'SKUName']]
Id2Name = dict(zip(skuIdName.loc[:,'ProductId'],skuIdName.loc[:,'SKUName']))

def report(path,output):
    '''
    report nums of each  class

    Arguements:
    ------------
    path:
    output:*.csv format 
    '''
    dirs = os.listdir(path)

    df = pd.DataFrame(columns=['Id','Num','Name'])

    for d in dirs:
        if os.path.isdir(os.path.join(path,d)):
            files = os.listdir(os.path.join(path,d))
            try:
                name = Id2Name[int(d)]
                print(name)
            except:
                name= d
            df.loc[df.shape[0]] = {'Id':d,'Num':len(files),'Name':name}

    df = df.sort_values('Num',ascending=False)
    print(df.shape[0])
    df.to_csv(output,index=False)

if __name__ =="__main__":
    args = parser.parse_args()
    path = args.folder
    output = args.output
    report(path,output)
