import tensorflow as tf
import io
import os
import scipy.misc
import numpy as np
import six
import time

from six import BytesIO
from PIL import Image, ImageDraw, ImageFont

from tqdm import tqdm
import time
import tensorflow as tf
from object_detection.utils import visualization_utils as viz_utils
from object_detection.utils import ops
import cv2
import argparse
from pathlib import Path
import pandas as pd
from object_detection.utils import label_map_util
from  shiqingTools import tools
parser = argparse.ArgumentParser()
parser.add_argument('-i','--saved-model-path',type=str,default=r'/home/haiyan_zeng/code/ABI_TT_model/20210114_group_models_inference_graph/saved_model')
parser.add_argument('-g','--label-map-path',type=str,default=r'/home/haiyan_zeng/code/ABI_TT_model/train_config/group_detection/labelmap_20210114.pbtxt')
parser.add_argument('-e','--image-dir',type=str,default='/home/haiyan_zeng/data/TestData/group/20210119_line')
parser.add_argument('-d','--destination-predict-image-dir',type=str,default='/home/haiyan_zeng/data/TestDataResult/20210119_line/images/')
parser.add_argument('-t','--threshold',type=float,default=0.8)
parser.add_argument('-p','--dest-csv-path',type=str,default='/home/haiyan_zeng/data/TestDataResult/20210119_line/20210119_line_group_pred.csv')

def load_image_into_numpy_array(path):
  img_data = tf.io.gfile.GFile(path, 'rb').read()
  image = Image.open(BytesIO(img_data))
  (im_width, im_height) = image.size
  return np.array(image.getdata()).reshape(
      (im_height, im_width, 3)).astype(np.uint8)


def detection_masks_to_images(detections, img_np):
    detection_masks_reframed = ops.reframe_box_masks_to_image_masks(
        detections['detection_masks'][0], detections['detection_boxes'][0],
        img_np.shape[0], img_np.shape[1])
    detection_masks_reframed = tf.cast(detection_masks_reframed > 0.5, tf.uint8)

    return detection_masks_reframed

def calculate_maskpolygon_label(detections,threshold,detection_masks_reframed,image_url,category_index):
    detection_object_num = np.sum((detections['detection_scores'][0].numpy() > threshold) * 1, axis=0)
    mask_binary = detection_masks_reframed.numpy()
    class_labels = detections['detection_classes'][0].numpy()

    single_image_url_list = []
    single_polygon_list = []
    single_class_list = []
    for i in range(detection_object_num):
        contours, hierarchy = cv2.findContours(mask_binary[i], cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
        (img_height,img_width) = np.shape(mask_binary[i])
        length = len(contours)
        for j in range(length):
            cnt = contours[j]
            epsilon = 0.02 * cv2.arcLength(cnt, True)
            approx = cv2.approxPolyDP(cnt, epsilon, True)
            approx = np.squeeze(approx).astype(float)
            try:
                approx[:,0] = approx[:,0]/img_width
                approx[:,1] = approx[:,1]/img_height
                single_image_url_list.append(image_url)
                single_polygon_list.append(np.squeeze(approx).tolist())
                single_class_list.append(category_index[class_labels[i]]['name'])
            except:
                pass

    return single_image_url_list,single_polygon_list,single_class_list

def save_predict_mask_image(img_np,detections,image_masks,destination_dir,category_index,img_path,min_threshold):
    image_np_with_detections = img_np.copy()
    viz_utils.visualize_boxes_and_labels_on_image_array(
        image_np_with_detections,
        detections['detection_boxes'][0].numpy(),
        detections['detection_classes'][0].numpy().astype(np.int32),
        detections['detection_scores'][0].numpy(),
        category_index,
        image_masks,
        use_normalized_coordinates=True,
        max_boxes_to_draw=200,
        min_score_thresh=min_threshold)
    
    img_path = img_path.replace('jpg','png')
    destination_path = os.path.join(destination_dir, img_path)
    viz_utils.save_image_array_as_png(image_np_with_detections, destination_path)
    #cv2.imwrite(destination_path,image_np_with_detections)

def inference_to_result_csv(args):
    """
    conduct the csv result ,then generate the evaluation result
    :param args:
    :return:
    """
    # step 1:load model
    detect_fn = tf.saved_model.load(args.saved_model_path)

    #step 2:read images,get the predict result,generate the csv files
    img_path_list = os.listdir(args.image_dir)
    image_url_list = []
    polygon_list = []
    product_id_list = []
    for img_path in tqdm(img_path_list):
        if '.jpg' in img_path:
            #conduct image url
            image_url = 'https://fileman.clobotics.cn/api/file/'+img_path[:img_path.index('.jpg')]
           #read images
            begin = time.time()
            img_np = load_image_into_numpy_array(os.path.join(args.image_dir, img_path))
            input_tensor = np.expand_dims(img_np, 0)
            #get the predict result
            detections = detect_fn(input_tensor)
            #get the image mask
            detection_masks_reframed = detection_masks_to_images(detections, img_np)
            image_masks = detection_masks_reframed.numpy()
            #save the predict bounding box and mask images for single image
            label_map = label_map_util.load_labelmap(args.label_map_path)
            categories = label_map_util.convert_label_map_to_categories(
                        label_map,
                            max_num_classes=label_map_util.get_max_label_map_index(label_map),
                                use_display_name=True)
            category_index = label_map_util.create_category_index(categories)
            label_map_dict = label_map_util.get_label_map_dict(label_map, use_display_name=True)
            #id2name = tools.get_id2name()
            #for k in category_index.keys():
            #    category_index[k]['name'] = id2name[int(category_index[k]['name'])][:-2]
            if not os.path.exists(args.destination_predict_image_dir):
                os.system("mkdir -vp {}".format(args.destination_predict_image_dir))
            save_predict_mask_image(img_np, detections, image_masks, args.destination_predict_image_dir, category_index, img_path,args.threshold)
            #calculate maskpolygon list
            single_image_url_list,single_polygon_list,single_class_list = calculate_maskpolygon_label(detections, args.threshold, detection_masks_reframed, image_url,category_index)
            end = time.time()
            #append the result list
            image_url_list.extend(single_image_url_list)
            polygon_list.extend(single_polygon_list)
            product_id_list.extend(single_class_list)
    #step3 save the predict result
    df = pd.DataFrame({'ImageUrl':image_url_list,'Polygon':polygon_list,'ProductId':product_id_list})
    df.to_csv(args.dest_csv_path,index=False)

def main(args):
    inference_to_result_csv(args)

if __name__ == '__main__':
    main(parser.parse_args())

