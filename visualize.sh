#!/bin/bash

#author:shiqing
#date:2021-01-18

mode=$1
model=$2
datatime=$3

if [ $# -ne 3 ];then
	echo "Usage:./visualize mode modeltime datatime"
	exit
fi 

python visualize_predict_result.py\
	--gt-csv-file /home/haiyan_zeng/data/TestData/${mode}/${datatime}_test/${datatime}_${mode}_test_gt.csv\
	--pred-csv-file /home/haiyan_zeng/data/TestDataResult/${mode}/${datatime}_test/${model}_model_${datatime}_test_result.csv\
	--img-dir /home/haiyan_zeng/data/TestData/${mode}/${datatime}_test/images\
	--dest-img-dir /home/haiyan_zeng/data/TestDataResult/${mode}/${datatime}_test/${model}_visualize
