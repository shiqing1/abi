#!/usr/bin/env python
#-*- coding:utf-8 -*-
"""
crop img
@Author:Zeng Haiyan
@Date:20201012
"""
import os
import argparse
from PIL import Image
import pandas as pd
import numpy as np
import urllib
from tqdm import  tqdm 
import urllib.request
from multiprocessing import Pool
import threading
from multiprocessing.pool import ThreadPool

parser = argparse.ArgumentParser()
parser.add_argument('-i','--src-csv-path',type=str,default=r'group_line_test.csv')
parser.add_argument('-d','--dest-img-dir',type=str,default=r'group_line_test')

def retry(attempt):
    def decorator(func):
        def wrapper(*args, **kw):
            att = 0
            while att < attempt:
                try:
                    return func(*args, **kw)
                except Exception as e:
                    att += 1
        return wrapper
    return decorator

@retry(attempt=3)
def get_image(img_url,data_dir):
    """
    download image
    :param data_dir:
    :param img_url:
    :param file_name:
    :return:
    """
    if '.jpg' in img_url:
        file_name = img_url[img_url.rindex('/') + 1:img_url.rindex('.')]
    else:
        file_name = img_url[img_url.rindex('/') + 1:]

    img_path = os.path.join(data_dir, file_name + '.jpg')
    if not os.path.exists(img_path):
        request = urllib.request.Request(img_url)
        response = urllib.request.urlopen(request)
        get_img = response.read()
        with open(img_path, 'wb') as fp:
            fp.write(get_img)
    print("done")

def download_image_thread(url_list, our_dir, num_processes=8, remove_bad=False, Async=True):
    '''
    多线程下载图片
    :param url_list: image url list
    :param our_dir:  保存图片的路径
    :param num_processes: 开启线程个数
    :param remove_bad: 是否去除下载失败的数据
    :param Async:是否异步
    :return: 返回图片的存储地址列表
    '''
    # 开启多线程
    if not os.path.exists(our_dir):
        os.makedirs(our_dir)
    pool = ThreadPool(processes=num_processes)
    thread_list = []
    for image_url in url_list:
        if Async:
            out = pool.apply_async(func=get_image, args=(image_url, our_dir))  # 异步
        else:
            out = pool.apply(func=get_image, args=(image_url, our_dir))  # 同步
        thread_list.append(out)
 
    pool.close()
    pool.join()
    # 获取输出结果
    image_list = []
    if Async:
        for p in thread_list:
            image = p.get()  # get会阻塞
            image_list.append(image)
    else:
        image_list = thread_list
    if remove_bad:
        image_list = [i for i in image_list if i is not None]
    return image_list


def main(args):


    df = pd.read_csv(args.src_csv_path)
    p = Pool(processes=64)
    path = args.dest_img_dir
    url_list = df['ImageUrl'].values.tolist()
    download_image_thread(url_list,path)
    
    
if __name__ == '__main__':
    main(parser.parse_args())


