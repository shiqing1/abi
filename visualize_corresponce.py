#!/usr/bin/env python
# -*- coding: utf-8 -*-
# @Time    : 2020/10/28 13:31
# @Author  : haiyan_zeng
# @File    : visualize_predict_result.py
import argparse
import pandas as pd
import cv2
import os
import numpy as np
from PIL import ImageDraw,ImageFont,Image
import math

parser = argparse.ArgumentParser()
parser.add_argument('-i','--analysize-csv-path',type=str,default=r'/home/haiyan_zeng/results/20201212_group/Correspondense.csv')
parser.add_argument('-s','--img-dir',type=str,default=r'/home/haiyan_zeng/data/TestData/group_detection/20201212_test/')
parser.add_argument('-d','--dest-img-dir',type=str,default='/home/haiyan_zeng/results/20201212_group/images')


def draw_planopolygon(polygons, labels, img, dst_path):
    """
    Args:
      polygons: [[pt_x, pt_y], ... ], each pt is an int number.
      labels: a list of sku label.
      img: array like cv2.imread

    Returns: None
    """
    metadata_path = r'/home/haiyan_zhang/Id_Name_metadata_20200921.csv'
    df = pd.read_csv(metadata_path)
    metadata_dict = dict(zip(df['Id'], df['Name']))
    img_array = copy.deepcopy(img)
    h, w, _ = img_array.shape
    color = [(0, 255, 0), (0, 0, 255), (255, 50, 0)]
    for r_i, polygon_pts in enumerate(polygons):
        # row_lpt = (int(row_pts_list[0][0]), int(row_pts_list[0][1]))
        polygon_pts = np.dot(polygon_pts, np.diag([w, h])).astype(dtype=np.int32)
        # print('polygon_pts', polygon_pts)
        center_pt_mean = np.mean(polygon_pts, axis=0).astype(dtype=np.int32)
        center_pt = np.min(polygon_pts, axis=0).astype(dtype=np.int32)
        center_pt = (center_pt[0].item(), center_pt_mean[1].item())
        cv2.polylines(img_array, [polygon_pts], 1, color[labels[r_i] % 3], thickness=3)
        fontColor = (0, 255, 255)
        font = cv2.FONT_HERSHEY_SIMPLEX
        fontScale = 1
        lineType = 2
        product_id = labels[r_i]
        try:
            show_str = '{}'.format(metadata_dict[product_id])
        except:
            show_str = str(product_id)
        # cv2.putText(img_array, show_str,
        #             center_pt,
        #             font,
        #             fontScale,
        #             fontColor,
        #             thickness=None,
        #             lineType=lineType,
        #             bottomLeftOrigin=False)

        # zhy add draw chinese
        # 导入字体文件
        fontpath = "chanyumengxinti.ttf"
        # 设置字体的颜色
        b, g, r, a = 0, 255, 0, 0
        # 设置字体大小
        font = ImageFont.truetype(fontpath, 18)
        # 将numpy array的图片格式转为PIL的图片格式
        img_pil = Image.fromarray(img_array)
        # 创建画板
        draw = ImageDraw.Draw(img_pil)
        # 在图片上绘制中文
        draw.text(center_pt, show_str, font=font, fill=(b, g, r, a))
        # 将图片转为numpy array的数据格式
        img_array = np.array(img_pil)
    cv2.imwrite(filename=dst_path, img=img_array)

def draw_polygon(img_array,polygon_pts,w,h,gt_id,pt_id,tmp_color):
    try:
        polygon_pts = eval(polygon_pts)
    except:
        return img_array
    polygon_pts = np.dot(polygon_pts, np.diag([w, h])).astype(dtype=np.int32)
    # print('polygon_pts', polygon_pts)
    center_pt_mean = np.mean(polygon_pts, axis=0).astype(dtype=np.int32)
    # center_pt = np.min(polygon_pts, axis=0).astype(dtype=np.int32)
    # center_pt = (center_pt[0].item(), center_pt[1].item())
    center_pt = (center_pt_mean[0].item(), center_pt_mean[1].item())
    #求解右下角
    center_pt_max_x = np.max(polygon_pts, axis=0).astype(dtype=np.int32)
    center_pt_max_y = np.max(polygon_pts, axis=1).astype(dtype=np.int32)
    center_pt_max = (center_pt_max_x[1].item(), center_pt_max_y[1].item())
    cv2.polylines(img_array, [polygon_pts], 1, tmp_color, thickness=3)

    try:
        gt_show_str = str(int(gt_id))
    except:
        gt_show_str = ''
    try:
        pt_show_str = str(int(pt_id))
    except:
        pt_show_str = ''

    # 导入字体文件
    fontpath = "SIMYOU.TTF"
    # 设置字体的颜色
    b, g, r, a = 0, 255, 0, 0
    # 设置字体大小
    font = ImageFont.truetype(fontpath, 18)
    # 将numpy array的图片格式转为PIL的图片格式
    img_pil = Image.fromarray(img_array)
    # 创建画板
    draw = ImageDraw.Draw(img_pil)
    # 在图片上绘制中文,左上角
    draw.text(center_pt, pt_show_str, font=font, fill=(b, g, r, a))
    # 右下角
    #draw.text(center_pt_max, gt_show_str, font=font, fill=(b, g, r, a))
    # 将图片转为numpy array的数据格式
    img_array = np.array(img_pil)


    return img_array

def visualize_predict_result(analysize_csv_path,img_dir,dest_img_dir):
    #read csv
    df = pd.read_csv(analysize_csv_path)

    #get the predict result
    df_img_group = df.groupby('ImageUrl')

    for img_url,tmp_img_group in df_img_group:
        gt_polygons_list = tmp_img_group['GtPolygon'].values
        predict_polygon_list = tmp_img_group['PtPolygon'].values
        predict_results = tmp_img_group['PredictId'].values
        gt_results = tmp_img_group['ProductId'].values

        img_name = img_url[img_url.rindex('/')+1:]+'.jpg'
        img_file_path = os.path.join(img_dir,img_name)
        img = cv2.imread(img_file_path)
        import matplotlib.pyplot as plt
        (img_height,img_width) = np.shape(img)[:2]
        for (index,predict) in enumerate(predict_results):
            if math.isnan(predict):
                #预测值为空，真实值有是蓝色
                img = draw_polygon(img, gt_polygons_list[index], img_width, img_height,gt_results[index],'', (0, 0, 255))
                continue

            try:
                if int(predict) == int(gt_results[index]):
                    #预测正确
                    img = draw_polygon(img, predict_polygon_list[index],img_width, img_height,gt_results[index],predict_results[index], (0, 255, 0))
                else:
                    #预测错误
                    img = draw_polygon(img, predict_polygon_list[index], img_width, img_height, gt_results[index],predict_results[index],(255, 0, 0))
            except:
                img = draw_polygon(img, predict_polygon_list[index], img_width, img_height, gt_results[index],predict_results[index], (255, 0, 0))

        #保存结果
        dest_img_path = os.path.join(dest_img_dir, img_name[:img_name.rindex('.jpg')] + '_p.jpg')
        cv2.imwrite(dest_img_path, img)
        print(dest_img_path)

def main(agrs):
    visualize_predict_result(agrs.analysize_csv_path,agrs.img_dir,agrs.dest_img_dir)

if __name__ == '__main__':
    main(parser.parse_args())
