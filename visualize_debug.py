#!/usr/bin/env python
# -*- coding: utf-8 -*-
# @Time    : 2020/10/28 13:31
# @Author  : haiyan_zeng
# @File    : visualize_predict_result.py
import argparse
import pandas as pd
import cv2
import os
import numpy as np
from PIL import ImageDraw,ImageFont,Image
import math
from tqdm import  tqdm 
from  shiqingTools import tools 

parser = argparse.ArgumentParser()
parser.add_argument('-g','--gt-csv-file',type=str,default=r'20201231_group_test_gt.csv')
parser.add_argument('-p','--pred-csv-file',type=str,default=r'20210114_model_20201231_test_result.csv')
parser.add_argument('-s','--img-dir',type=str,default=r'/home/zyl/workspace/ABI/group_data/20201231/csv/20201231_group_test')
parser.add_argument('-d','--dest-img-dir',type=str,default=r'/home/zyl/workspace/performance/20210114_visualize')
parser.add_argument('-b','--debug-csv',type=str,default=r'debug_results.csv')


def draw_polygon(img_array,polygon_pts,w,h,id,is_gt=False):

    if is_gt:#red 
        tmp_color = (0,0,255)
        b, g, r, a = 0, 0, 255, 0
    else:#blue
        tmp_color = (255,0,0)
        b, g, r, a = 255, 0, 0, 0
    try:
        polygon_pts = eval(polygon_pts)
    except:
        return img_array
    polygon_pts = np.dot(polygon_pts, np.diag([w, h])).astype(dtype=np.int32)
    # print('polygon_pts', polygon_pts)

    center_pt_mean = np.mean(polygon_pts, axis=0).astype(dtype=np.int32)
    center_pt = (center_pt_mean[0].item(), center_pt_mean[1].item())
    #求解右下角
    cv2.polylines(img_array, [polygon_pts], 1, tmp_color, thickness=3)

    # 导入字体文件
    fontpath = "SIMYOU.TTF"
    # 设置字体的颜色

    # 设置字体大小
    font = ImageFont.truetype(fontpath, 30)
    # 将numpy array的图片格式转为PIL的图片格式
    img_pil = Image.fromarray(img_array)
    # 创建画板
    draw = ImageDraw.Draw(img_pil)
    # 在图片上绘制中文,左上角

    if is_gt:
        id2name = tools.get_id2name()
        show_str = "GT:"+id2name[id].replace('堆箱','')
        draw.text(center_pt, show_str, font=font, fill=(b, g, r, a))
    else:
        show_str = "Pred:"+id
        center_pt_ = (center_pt[0],max(center_pt[1]+30,0))
        draw.text(center_pt_, show_str, font=font, fill=(b, g, r, a))   

    img_array = np.array(img_pil)


    return img_array

def visualize_predict_result(img_dir,dest_img_dir,gt_csv_file,predict_csv_file,debug_csv):

    df = pd.read_csv(debug_csv)
    df = df[df['EvalResult']!='correct']
    names = list(df['ImageName'])

    files = os.listdir(img_dir)
    gt_df = pd.read_csv(gt_csv_file)
    pred_df = pd.read_csv(predict_csv_file)

    files = os.listdir(img_dir)
    
    for f in tqdm(files):
        if f not in names:
            continue
        img_path = os.path.join(img_dir,f)
        img = cv2.imread(img_path)
        (h,w) = np.shape(img)[:2]
        image_id = f[:-4]

        gt = gt_df[gt_df['ImageUrl'].str.contains(image_id)]
        for i in range(gt.shape[0]):
            row = gt.iloc[i]
            gt_polygon = row['Polygon']
            gt_id = row['ProductId']
            img = draw_polygon(img,gt_polygon,w,h,gt_id,True)#ground truth red color 
        pred = pred_df[pred_df['ImageUrl'].str.contains(image_id)]
        for i in range(pred.shape[0]):
            row = pred.iloc[i]
            pred_polygon = row['Polygon']
            pred_id = row['ProductId']
            img = draw_polygon(img,pred_polygon,w,h,pred_id,False)#pred blue color 
        
        dst_path = os.path.join(dest_img_dir,f)
        cv2.imwrite(dst_path,img)

    

def main(args):
    visualize_predict_result(args.img_dir,args.dest_img_dir,args.gt_csv_file,args.pred_csv_file,args.debug_csv)

if __name__ == '__main__':
    main(parser.parse_args())
