import argparse
import codecs
import json

parser = argparse.ArgumentParser()
parser.add_argument('-i','--input-json-file-path',type=str,required=True,default='')
parser.add_argument('-o','--output-pbtxt-file-path',type=str,default='./labelmap.pbtxt')

def main(args):
    with open(args.input_json_file_path,'r') as f:
        json_file = json.load(f)
        json_categories = json_file['categories']

        with codecs.open(args.output_pbtxt_file_path,'w',encoding='utf-8') as cf:
            for category in json_categories:
                ml_id = int(category['id'])+1
                pms_id = int(category['name'])
                cf.write('item {{\n  id: {0}\n  name: \'{1}\'\n  \n}}\n'.format(ml_id,pms_id))


if __name__ == "__main__":
    main(parser.parse_args())
