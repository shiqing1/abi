import argparse
import json



parser = argparse.ArgumentParser()
parser.add_argument('-m','--mode',required=True)
parser.add_argument('-t','--time',required=True)
args = parser.parse_args()
mode = args.mode
time = args.time


labelmap_file = './train_config/{}_detection/labelmap_{}.pbtxt'.format(mode,time)
with open(labelmap_file,'r') as f:
    line = f.readlines()[-4]
    N = int(line.split(' ')[-1][:-1])

with open('./train_config/mask_rcnn_inception_resnet_v2_1024x1024_coco17_gpu-8.config','r') as f:
    lines = f.readlines()

    #配置参数
    lines[3] = '    num_classes:{}\n'.format(N)
    lines[125] = '  label_map_path: "/home/haiyan_zeng/code/ABI_TT_model/train_config/{}_detection/labelmap_{}.pbtxt"\n'.format(mode,time)
    lines[127] = '    input_path:"/home/haiyan_zeng/data/TrainData/tfrecord/{}_detection/{}/train.record"\n'.format(mode,time)
    lines[143] = '  label_map_path: "/home/haiyan_zeng/code/ABI_TT_model/train_config/{}_detection/labelmap_{}.pbtxt"\n'.format(mode,time)
    lines[147] = '    input_path:"/home/haiyan_zeng/data/TrainData/tfrecord/{}_detection/{}/test.record"\n'.format(mode,time)

model_file = './train_config/{}_detection/mask_rcnn_inception_resnet_v2_1024x1024_coco17_gpu-8_{}.config'.format(mode,time)
with open(model_file,'w') as g:
    g.write(''.join(lines))



