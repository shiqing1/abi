#!/bin/bash 

set -e
# set -x 

mode=$1
time=$2

if [ $# -ne 2 ];then
        echo "Usage:./augment_and_merge.sh  mode[group/single] time"
        exit
fi
parent=/home/zyl/workspace/ABI/${mode}_data/${time}
if [ ! -d $parent ]; then
    mkdir -p $parent
fi

if [ ! -f ${parent}/csv/${time}_${mode}_src_filter.csv ];then
    echo "Error:${parent}/csv/${time}_${mode}_src_filter.csv not exist!Please check again."
    exit 
fi


#分割train/test
echo "split train/test..."
python data_tools/split_train_test_csv.py -i  ${parent}/csv/${time}_${mode}_src_filter.csv  -tr ${parent}/csv/${time}_${mode}_train.csv -te ${parent}/csv/${time}_${mode}_test.csv
#对训练集数据增强
echo "data augmentation..."
python data_tools/image_augmentation.py -i ${parent}/csv/${time}_${mode}_train.csv -d ${parent}/augmentation_images  -s ${parent}/augmentation_csv
#合并csv
echo "augmentation csv merge..."
python data_tools/merge_dir.py -d ${parent}/augmentation_csv  -o ${parent}/aug_combined.csv
#上传增强后的图片
echo "upload augmentation images..."
python image_tools/batch_resize_upload_images_x.py -i ${parent}/augmentation_images -d ${parent}/upload
#得到csv文件
echo "get augmentation images csv..."
python data_tools/map_upload_augmentation.py -a ${parent}/aug_combined.csv  -u ${parent}/upload/success_upload.csv -d ${parent}/aug_mapped.csv
#合并csv,得到最终结果(这是本次增量的csv)
echo "merge origin and augmentation csv"
python data_tools/merge_csv.py -f1 ${parent}/aug_mapped.csv -f2 ${parent}/csv/${time}_${mode}_train.csv -o ${parent}/${time}_${mode}_train_increment.csv
#得到本次训练集(这是本次增量的训练集)
echo "get training set..."
python  image_tools/labman_to_labelme.py -i ${parent}/${time}_${mode}_train_increment.csv  -o ${parent}/train  -s ${parent}/train

