import argparse
import os
import cv2
import time
import random
import numpy as np
import requests
import base64
import logging
from tqdm import tqdm
from multiprocessing.pool import ThreadPool
import Protobuf.internal.services.fileman.fileman_pb2 as fileman

def parse_args():
  parser = argparse.ArgumentParser(description='resize and upload images')
  parser.add_argument('-i', '--image_path', type=str,default=r'/home/zyl/workspace/ABI/group_data/20201231/augmentation_images')
  parser.add_argument('-d', '--data_path', type=str,default=r'/home/zyl/workspace/ABI/group_data/20201231/upload')
  parser.add_argument('-uw', '--upload_worker', type=int, default=50)
  #parser.add_argument('-h', '--height', type=int, default=1920)
  #parser.add_argument('-w', '--weight', type=int, default=1080)
  return parser.parse_args()

args = parse_args()
if not os.path.exists(args.data_path):
  os.makedirs(args.data_path)

time_str = time.strftime("%Y%m%d%H%M")

def _init_logging():
  log_path = os.path.join(args.data_path, 'log_' + time_str + '.log')
  logging.getLogger().setLevel(logging.INFO)
  logFormatter = logging.Formatter("%(asctime)s [%(threadName)-12.12s] [%(levelname)-5.5s]  %(message)s")
  fileHandler = logging.FileHandler(log_path)
  fileHandler.setFormatter(logFormatter)

  rootLogger = logging.getLogger()
  rootLogger.addHandler(fileHandler)

  consoleHandler = logging.StreamHandler()
  consoleHandler.setFormatter(logFormatter)
  rootLogger.addHandler(consoleHandler)

def iterate_dir(dir):
  image_path_list = []
  for i in os.listdir(dir):
    if os.path.isfile(os.path.join(dir, i)):
      if i.endswith('.jpg'):
        image_path_list.extend([os.path.join(dir, i)])
      else:
        pass
    else:
      image_path_list.extend(iterate_dir(os.path.join(dir, i)))
  return image_path_list


# ori is 3840 * 2160
# app 1280 * 720
def resize_img(img, height, width):
  ori_height, ori_width, _ = img.shape

  if ori_height < height or ori_width < width:
    return img
  else:
    height_ratio = height * 1.0 / ori_height
    width_ratio = width * 1.0 / ori_width
    ratio = max(height_ratio, width_ratio)
    resized_img = cv2.resize(img, (0, 0), fx=ratio, fy=ratio)
    return resized_img

def resize_upload(image_path, height=1920, width=1080,  n_retry=50):
  img = cv2.imread(image_path)
  if img is None:
    logging.error(image_path + 'is empty !')
    return '', None, None

  #resized_image = resize_img(img, height, width)
  resized_image = img

  resized_image_dir = os.path.join(args.data_path,
                                   'images',
                                   os.path.basename(os.path.dirname(image_path)))

  if not os.path.exists(resized_image_dir):
    try:
      os.makedirs(resized_image_dir)
    except:
      pass
  _ = cv2.imwrite(os.path.join(resized_image_dir, os.path.basename(image_path)), resized_image)

  strImage=cv2.imencode('.jpg', resized_image)[1]
  data_encode = np.array(strImage)
  str_encode = data_encode.tostring()
  str_image = base64.b64encode(str_encode)

  # Construct protobuf upload request
  token = "Q2xvYm90aWNzLlJldGFpbC5CaXpNYW4uRmxvd01hbg=="
  upload_request = fileman.UploadFileRequest()
  upload_request.file_content = str_image
  upload_request.access_token = token
  upload_request.upload_file_info.name = 'test10.jpg'
  upload_request.upload_file_info.where_from = fileman.WhereFrom.ALGOMAN
  upload_request.upload_file_info.longitude = 0
  upload_request.upload_file_info.latitude = 0
  upload_request.upload_file_info.device_orientation = 0
  upload_request.upload_file_info.file_type = fileman.FileType.IMAGE

  # Upload the image file
  AddFileManURL = "https://fileman.clobotics.cn/api/add/base64"
  Header = {'content-type': 'application/x-protobuf', 'accept': 'application/json', 'charset': 'utf-8'}

  for _ in range(n_retry):
    try:
      response = requests.put(AddFileManURL, data=upload_request.SerializeToString(),
                              headers=Header, timeout=300)
      message = response.json()
      file_id = message['FileId']
      if not file_id:
        logging.error('{} Upload fileman return none {} times!'.format(image_path, _))
        time.sleep(random.random())
        continue
      else:
        c_url = 'https://fileman.clobotics.cn/api/file/' + file_id
        return c_url, img.shape, resized_image.shape
    except:
      if _ == n_retry - 1:
        logging.error('{} Upload failed finally!'.format(image_path))
        return '', None, None
      else:
        time.sleep(random.random())
  return '', None, None


def main():
  _init_logging()
  f_success_path = os.path.join(args.data_path, 'success_upload' + '.csv')
  f_fail_path = os.path.join(args.data_path, 'fail_' + time_str + '.csv')
  with open(f_success_path, 'w') as f:
    f.write('ImgUrl,ImgPath\n')
  with open(f_fail_path, 'w') as f:
    f.write('ImgPath\n')


  if not os.path.exists(os.path.join(args.data_path, 'image_path.txt'))\
      or not os.path.exists(os.path.join(args.data_path, 'process_id')):

    image_path_list = iterate_dir(args.image_path)

    with open(os.path.join(args.data_path, 'image_path.txt'), 'w') as f:
      for i in image_path_list:
        f.write(i + '\n')

    processed_id = -1
    with open(os.path.join(args.data_path, 'processed_id'), 'w') as f:
      f.write(str(processed_id) + '\n')

  else:
    try:
      with open(os.path.join(args.data_path, 'image_path.txt'), 'r') as f:
        image_path_list = f.readlines()
        image_path_list = [i.strip() for i in image_path_list]
    except:
      logging.error('image_path write error!')
      raise ValueError('image_path write error!')
    try:
      with open(os.path.join(args.data_path, 'processed_id'), 'r') as f:
        processed_id = int(f.readline().strip())
    except:
      logging.error('processed id write error!')
      raise ValueError('processed id write error!')

  pool = ThreadPool(processes=args.upload_worker)

  res_image_path_list = image_path_list[processed_id + 1:]

  image_path_batch_list = [res_image_path_list[i: i + args.upload_worker]
                           for i in range(0, len(res_image_path_list), args.upload_worker)]

  for one_image_path_batch in tqdm(image_path_batch_list,
                                   ascii=True,
                                   desc='Enumerate each image path batch'):
    batch_res = pool.map(resize_upload, one_image_path_batch)
    processed_id = len(one_image_path_batch) + processed_id
    with open(os.path.join(args.data_path, 'processed_id'), 'w') as f:
      f.write(str(processed_id) + '\n')

    f_success = open(f_success_path, 'a')
    f_fail = open(f_fail_path, 'a')
    for one_image_path, url_orishape_resizeshape in zip(one_image_path_batch, batch_res):
      if not url_orishape_resizeshape[0]:
        f_fail.write(one_image_path + '\n')
      else:
        f_success.write('{},{}\n'.format(url_orishape_resizeshape[0],one_image_path))

    f_success.close()
    f_fail.close()


if __name__=='__main__':
  main()
