#!/usr/bin/env python
#-*- coding:utf-8 -*-
"""
download images
@Author:Zeng Haiyan
@Date:20200907
"""
import pandas as pd
import urllib
import urllib.request
import os

def download_image(img_url,destination_dir):
    """
    download image through image url
    :param img_url:
    :param destination_dir:
    :return: local image path
    """

    #some records have .jpg;we should get the image path to save
    if '.jpg' in img_url:
        img_name = img_url[img_url.rindex('/') + 1:img_url.rindex('.')]
    else:
        img_name = img_url[img_url.rindex('/') + 1:len(img_url)]
    if not os.path.exists(destination_dir):
        os.mkdir(destination_dir)


    image_file_path = os.path.join(destination_dir,img_name+'.jpg')
    if not os.path.exists(image_file_path):
        #download to image path
        request = urllib.request.Request(img_url)
        response = urllib.request.urlopen(request)
        get_img = response.read()
        with open(image_file_path, 'wb') as fp:
            fp.write(get_img)

    return image_file_path
