#!/usr/bin/env python
#-*- coding:utf-8 -*-
"""
create gray mask for polygon label image
Author:Zeng Haiyan
Date:20200901
"""
import argparse
import cv2
import numpy as np
import pandas as pd
from pathlib import Path
import os
import urllib
import urllib.request
import matplotlib.pyplot as plt
from skimage import io

parser = argparse.ArgumentParser()
parser.add_argument('-i','--src-csv-path',type=Path,default=Path(r'D:\000-clobotics-workspace\CV2020P08-ABI-TT\002-data\003-model-train-test-data\TT-channel\20200923-TestData\374204_ee7dbf2c-856d-48ad-95ad-ad15d1741ce9_result.csv'))
parser.add_argument('-g','--src-img-dir',type=str,default=r'D:\000-clobotics-workspace\CV2020P08-ABI-TT\002-data\003-model-train-test-data\TT-channel\20200923-TestData\images-gray-except-sku')

def main(args):
    """
    main function
    :param args:
    :return:
    """
    batch_generate_gray_mask(args.src_img_dir,args.src_csv_path)

def batch_generate_gray_mask(src_img_dir,src_csv_path):

    #read batch csv file from directory
    df = pd.concat([pd.read_csv(p) for p in _get_file_path(Path(src_csv_path))])

    #group by image url
    img_url_df = df.groupby('ImageUrl')

    #iterate whole dataset to get mask of image which have the confusion area
    for img_url,group_data in img_url_df:
        polygon_list = group_data.loc[(group_data['ProductId'] == 1104881) | (group_data['ProductId'] == 1047936)]['Polygon'].values

        #if there exists confusion area,we should add gray mask
        if len(polygon_list)>0:
            img_path = get_image(src_img_dir, img_url)
            print(img_path)
            get_gray_mask_image(img_path,polygon_list)

def _get_file_path(path):
    """
    get file path list of dir
    :param path:
    :return:
    """
    if path.is_dir():
        return sorted(path.rglob("*.csv"))
    return [path]

def get_image(data_dir, img_url):
    """
    download image
    :param data_dir:
    :param img_url:
    :param file_name:
    :return:
    """
    if '.jpg' in img_url:
        file_name = img_url[img_url.rindex('/') + 1:img_url.rindex('.')]
    else:
        file_name = img_url[img_url.rindex('/') + 1:]

    img_path = os.path.join(data_dir, file_name + '.jpg')
    if not os.path.exists(img_path):
        request = urllib.request.Request(img_url)
        response = urllib.request.urlopen(request)
        get_img = response.read()
        with open(img_path, 'wb') as fp:
            fp.write(get_img)
    return img_path

def get_str_polygon_to_array(polygon,w,h):
    """
    get array of str type array
    :param polygon:
    :param w: the array need be enlarge by multiply width and hight
    :param h:the array need be enlarge by multiply width and hight
    :return:
    """
    polygon_str_list = polygon.replace('[', '').replace(']', '').split(',')
    polygon_number_list = list(map(float, polygon_str_list))
    polygon_arr = np.array(polygon_number_list)
    polygon_arr = np.reshape(polygon_arr, (-1, 2))
    polygon_arr[:, 0] = polygon_arr[:, 0] * h
    polygon_arr[:, 1] = polygon_arr[:, 1] * w
    polygon_arr = polygon_arr.astype(int)

    return polygon_arr

def get_gray_mask_image(img_path,polygon_list):
    """
    get one gray mask of a polygon image
    :param img_path: the path of orignal image
    :param polygon: labelling polygon
    :return: save gray mask image
    """
    #read original image
    origin_img = io.imread(img_path)

    #get size of image
    (w,h,t) = np.shape(origin_img)

    #total polygon list of an image
    total_polygon_list = []
    for polygon in polygon_list:
        polygon_arr = get_str_polygon_to_array(polygon,w,h)
        total_polygon_list.append(polygon_arr)

    #generate mask image according to the polygon
    mask_img = np.zeros(np.shape(origin_img))
    mask_img = cv2.polylines(mask_img, total_polygon_list, True, (255, 255, 255))
    mask_fill = cv2.fillPoly(mask_img, total_polygon_list, (255, 255, 255))

    mask_fill_bool = mask_fill>0
    mask_fill_bool_negate = ~ mask_fill_bool

    mask_black = origin_img*mask_fill_bool_negate
    mask_gray = mask_fill_bool*(origin_img.mean(axis=0).mean(axis=0)[None, None, :].astype(np.uint8))

    #generate new image and save it
    new_img = mask_black+mask_gray
    new_img_path = img_path[:img_path.index('.jpg')]+'.jpg'#_gray
    io.imsave(new_img_path,new_img)


if __name__ == '__main__':
    main(parser.parse_args())