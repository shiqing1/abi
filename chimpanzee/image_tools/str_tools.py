import numpy as np

def get_str_polygon_to_list(polygon, width, height):
    """
    get array of str type array
    :param polygon:
    :param width: the array need be enlarge by multiply width and hight
    :param height:the array need be enlarge by multiply width and hight
    :return:
    """
    polygon_str_list = polygon.replace('[', '').replace(']', '').split(',')
    polygon_number_list = list(map(float, polygon_str_list))
    polygon_arr = np.array(polygon_number_list)
    polygon_arr = np.reshape(polygon_arr, (-1, 2))
    polygon_arr[:, 0] = polygon_arr[:, 0] * width
    polygon_arr[:, 1] = polygon_arr[:, 1] * height
    #polygon_arr = polygon_arr.astype(int)

    return polygon_arr.tolist()
