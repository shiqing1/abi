#!/usr/bin/env python
# -*- coding: utf-8 -*-
# @Time    : 2020/10/22 16:11
# @Author  : haiyan_zeng
# @File    : delete_out_polygon.py
import argparse
import cv2
import numpy as np
import pandas as pd
from pathlib import Path
import os
import urllib
import urllib.request
import matplotlib.pyplot as plt
from skimage import io

parser = argparse.ArgumentParser()
parser.add_argument('-i','--src-csv-path',type=Path,default=Path(r'D:\000-clobotics-workspace\CV2020P08-ABI-TT\002-data\002-data-intermediate-processing\20201021-TT-batch1-dataprocess\step7_total_csv'))
parser.add_argument('-g','--src-img-dir',type=str,default=r'D:\000-clobotics-workspace\CV2020P08-ABI-TT\002-data\002-data-intermediate-processing\20201021-TT-batch1-dataprocess\step7_total_resized_images')
parser.add_argument('-d','--dest-csv-dir',type=str,default=r'D:\000-clobotics-workspace\CV2020P08-ABI-TT\002-data\002-data-intermediate-processing\20201021-TT-batch1-dataprocess\step7p5_total_csv')

def main(args):
    """
    main function
    :param args:
    :return:
    """
    batch_deal_polygon(args.src_img_dir, args.src_csv_path,args.dest_csv_dir)

def batch_deal_polygon(src_img_dir, src_csv_path, dest_csv_dir):

    #read batch csv file from directory
    df = pd.concat([pd.read_csv(p) for p in _get_file_path(Path(src_csv_path))])

    #group by image url
    img_url_df = df.groupby('ImageUrl')

    final_polygon_list = []
    final_productid_list = []
    final_img_list = []
    #iterate whole dataset to get mask of image which have the confusion area
    for img_url,group_data in img_url_df:
        tmp_rows = group_data.loc[(group_data['ProductId'] == 1104881) | (group_data['ProductId'] == 1083305) | (group_data['ProductId']==1109383)]
        polygon_list = tmp_rows['Polygon'].values
        productid_list = tmp_rows['ProductId'].values
        #if there exists confusion area,we should add gray mask
        if len(polygon_list)>0:
            img_path = get_image(src_img_dir, img_url)
            print(img_path)
            new_polygon_list,new_productid_list = get_new_polygon_list(img_path, polygon_list,productid_list)
            final_polygon_list.extend(new_polygon_list)
            final_productid_list.extend(new_productid_list)
            final_img_list.extend([img_url for i in range(len(new_polygon_list))])

    new_df = pd.DataFrame({'ImageUrl': final_img_list, 'Polygon': final_polygon_list,'ProductId': final_productid_list})
    new_df.to_csv(os.path.join(dest_csv_dir, 'total.csv'), index=False)

def _get_file_path(path):
    """
    get file path list of dir
    :param path:
    :return:
    """
    if path.is_dir():
        return sorted(path.rglob("*.csv"))
    return [path]

def get_image(data_dir, img_url):
    """
    download image
    :param data_dir:
    :param img_url:
    :param file_name:
    :return:
    """
    if '.jpg' in img_url:
        file_name = img_url[img_url.rindex('/') + 1:img_url.rindex('.')]
    else:
        file_name = img_url[img_url.rindex('/') + 1:]

    img_path = os.path.join(data_dir, file_name + '.jpg')
    if not os.path.exists(img_path):
        request = urllib.request.Request(img_url)
        response = urllib.request.urlopen(request)
        get_img = response.read()
        with open(img_path, 'wb') as fp:
            fp.write(get_img)
    return img_path

def get_str_polygon_to_array(polygon,w,h):
    """
    get array of str type array
    :param polygon:
    :param w: the array need be enlarge by multiply width and hight
    :param h:the array need be enlarge by multiply width and hight
    :return:
    """
    polygon_str_list = polygon.replace('[', '').replace(']', '').split(',')
    polygon_number_list = list(map(float, polygon_str_list))
    polygon_arr = np.array(polygon_number_list)
    polygon_arr = np.reshape(polygon_arr, (-1, 2))
    polygon_arr[:, 0] = polygon_arr[:, 0] * h
    polygon_arr[:, 1] = polygon_arr[:, 1] * w
    polygon_arr = polygon_arr.astype(int)

    return polygon_arr

def get_one_image_polygon(origin_img,total_polygon_list,w,h):
    # generate mask image according to the polygon
    mask_zero_img = np.zeros(np.shape(origin_img))
    orig_zero_img = np.copy(mask_zero_img)
    mask_img = cv2.polylines(mask_zero_img, total_polygon_list, True, (255, 255, 255))
    mask_fill = cv2.fillPoly(mask_img, total_polygon_list, (255, 255, 255))

    mask_fill_bool = (mask_fill > 0) * 255
    single_mask = mask_fill_bool[:, :, 0]

    mask_black = np.ones((w, h)) * single_mask
    mask_black = np.array(mask_black, np.uint8)
    # plt.imshow(mask_black)
    # plt.show()

    contours, hierarchy = cv2.findContours(mask_black, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)

    length = len(contours)
    cv2.drawContours(origin_img, contours, -1, (0, 255, 0), 3)
    # plt.imshow(origin_img)
    # plt.show()
    # cv2.imshow('contour', origin_img)
    # cv2.waitKey(1)

    approx_list = []
    for j in range(length):
        cnt = contours[j]
        epsilon = 0.01 * cv2.arcLength(cnt, True)
        approx = cv2.approxPolyDP(cnt, epsilon, True)
        approx = np.squeeze(approx).astype(float)
        return approx

def get_new_polygon_list(img_path, polygon_list,productid_list):
    """
    get one gray mask of a polygon image
    :param img_path: the path of orignal image
    :param polygon: labelling polygon
    :return: save gray mask image
    """
    #read original image
    origin_img = io.imread(img_path)

    #get size of image
    (w,h,t) = np.shape(origin_img)
    #total polygon list of an image
    total_polygon_list = []
    #重新构造的polygon
    new_polygon_list = []
    new_productid_list = []

    tmp_index = 0
    for tmp_index,polygon in enumerate(polygon_list):
        print(polygon)
        #check whether there is out.如果框画出去了，就重新处理这个框。
        polygon_arr = get_str_polygon_to_array(polygon,w,h)
        if np.sum(polygon_arr<0) > 0:
            #可以记录标签
            polygon_arr = get_one_image_polygon(origin_img,[polygon_arr],w,h)
        #将多边形还原
        try:
            polygon_arr = polygon_arr.astype(float)
            polygon_arr[:, 0] = polygon_arr[:, 0] / h
            polygon_arr[:, 1] = polygon_arr[:, 1] / w

            final_polygon_list = polygon_arr.tolist()
            new_polygon_list.append(final_polygon_list)
            new_productid_list.append(productid_list[tmp_index])
        except:
            tmp_index += 1
            #如果转错了，这个记录暂时不要
            pass
    print(tmp_index)
    return new_polygon_list,new_productid_list

if __name__ == '__main__':
    main(parser.parse_args())
