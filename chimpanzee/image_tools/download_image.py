#!/usr/bin/env python
#-*- coding:utf-8 -*-
"""
download images
Author:Zeng Haiyan
Date:20200915
"""
import pandas as pd
import urllib
import urllib.request
import os
import argparse
from pathlib import Path
import tqdm

parser = argparse.ArgumentParser()
parser.add_argument('-i','--input-csv-dir',type=Path,default=Path(r'D:\000-clobotics-workspace\CV2020P08-ABI-TT\002-data\002-data-intermediate-processing\00000002-create-task\20201105-TT-select-10000-to-create-task.csv'))
parser.add_argument('-g','--dest-image-dir',type=str,default=r'D:\000-clobotics-workspace\CV2020P08-ABI-TT\002-data\002-data-intermediate-processing\00000002-create-task\20201105-TT-10000-aother-images')
parser.add_argument('-e','--except-image-csv-path',type=str,default='eval_images.csv')

def _get_file_path(path):
    """
    get file path list of dir
    :param path:
    :return:
    """
    if path.is_dir():
        return sorted(path.rglob("*.csv"))
    return [path]

def download_image(input_csv_dir,dest_image_dir,except_image_csv_path):
    if not os.path.exists(dest_image_dir):
        os.mkdir(dest_image_dir)

    df = pd.concat([pd.read_csv(path) for path in _get_file_path(input_csv_dir)])
    img_url_list = df['ImageUrl'].unique()

    except_df_imageurl_list = []
    if except_image_csv_path != '':
        except_df = pd.read_csv(except_image_csv_path)
        except_df_imageurl_list = except_df['ImageUrl'].values.tolist()
        except_df_imageurl_list = [imgurl[imgurl.rindex('/')+1:] for imgurl in except_df_imageurl_list]

    for img_url in tqdm.tqdm(img_url_list):
        try:
            if '.jpg' in img_url:
                file_name = img_url[img_url.rindex('/') + 1:img_url.rindex('.')]
            else:
                file_name = img_url[img_url.rindex('/')+1:len(img_url)]
            print(file_name)

            if file_name not in except_df_imageurl_list:
                get_image(dest_image_dir,img_url,file_name)
            else:
                print('I have download!!!')
        except:
            print("Wrong!!!")

def main(args):
    download_image(args.input_csv_dir,args.dest_image_dir,args.except_image_csv_path)


def get_image(data_path, img_url, file_name):
    """
    download images
    :param data_path:
    :param img_url:
    :param file_name:
    :return:
    """
    tmp_file_path = os.path.join(data_path,file_name + '.jpg')
    if not os.path.exists(tmp_file_path):
        request = urllib.request.Request(img_url)
        response = urllib.request.urlopen(request)
        get_img = response.read()
        with open(tmp_file_path, 'wb') as fp:
            fp.write(get_img)

if __name__ == '__main__':
    main(parser.parse_args())
