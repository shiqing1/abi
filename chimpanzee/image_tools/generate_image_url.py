import os
import pandas as pd
import argparse
import random

# parser = argparse.ArgumentParser()
# parser.add_argument("--img-dir", type=str, required=True)
# parser.add_argument("--img-url-path", type=str, required=True)
# parser.add_argument("--prefix", type=str, default='')

class Args:
    def __init__(self, img_dir, img_url_path):
        self.img_dir = img_dir
        self.img_url_path = img_url_path
        self.prefix = 'https://fileman.clobotics.cn/api/file/'
        self.select_img_num = 130

def generate_img_url(img_dir,img_url_path,prefix,select_img_num):
    """
    根据图像文件生成图像url文件,选择特定的数目，需要指定
    :param img_dir:
    :param img_url_path:
    :param prefix:
    :param select_img_num:
    :return:
    """
    img_path_list = os.listdir(img_dir)
    img_path_list = [(prefix+path[0:path.rindex('.jpg')]) for path in img_path_list]

    # img_len = len(img_path_list)
    # random_list = random.sample(range(1, img_len), select_img_num)
    # img_url_list = [img_path_list[tmp_random] for tmp_random in random_list]

    data = pd.DataFrame()
    #data['img_url'] = img_url_list
    data['img_url'] = img_path_list

    data.to_csv(img_url_path,index=False)

def main(args):
    generate_img_url(args.img_dir,args.img_url_path,args.prefix,args.select_img_num)


if __name__ == '__main__':

    # img_dir = r'D:\000-clobotics-workspace\002-data\20200915-select-data-to-label'
    # img_url_path = r'D:\000-clobotics-workspace\002-data\20200915-select-labelling-image.csv'
    img_dir = r'D:\000-clobotics-workspace\CV2020P07-ABI-CR\create_label_task\image'
    img_url_path = r'D:\000-clobotics-workspace\CV2020P07-ABI-CR\create_label_task\select_300_CR_test.csv'
    args = Args(img_dir,img_url_path)

    main(args)
