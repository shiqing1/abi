import cv2
import numpy as np
import os
import argparse

parser = argparse.ArgumentParser()
parser.add_argument('-i','--src-img-dir',type=str,default=r'D:\000-clobotics-workspace\CV2020P08-ABI-TT\002-data\002-data-intermediate-processing\20201009-TT-traindata\origin_csv\376352_01afe0ef-153c-41a3-a1e8-601910e8cd6a_result.csv')
parser.add_argument('-g','--dest-img-dir',type=str,default=r'D:\000-clobotics-workspace\CV2020P08-ABI-TT\002-data\002-data-intermediate-processing\20201009-TT-traindata\376352_origin_images')
parser.add_argument('-e','--long-side',type=int,default=800)

def resize_img_keep_ratio_accord_longside(img, dest_long_size):
    """
    resize image according to the longsize
    :param img:
    :param dest_long_size:
    :return:
    """
    #get the long side of image
    img_height,img_width = np.shape(img)[:2]
    if img_width > img_height:
        ratio = dest_long_size/img_width
        new_img_width = dest_long_size
        new_img_height = int(img_height*ratio)
    else:
        ratio = dest_long_size /img_height
        new_img_height = dest_long_size
        new_img_width = int(img_width*ratio)

    return cv2.resize(img,(new_img_width,new_img_height))

def batch_resize_img_keep_ratio(src_img_dir,dest_img_dir,long_side):
    src_img_path_list = os.listdir(src_img_dir)
    for src_img_path in src_img_path_list:
        old_img = cv2.imread(os.path.join(src_img_dir,src_img_path))
        new_img = resize_img_keep_ratio_accord_longside(old_img, long_side)
        cv2.imwrite(os.path.join(dest_img_dir,src_img_path),new_img)

def main(args):
    batch_resize_img_keep_ratio(args.src_img_dir,args.dest_img_dir,args.long_side)

if __name__ == '__main__':
    main(parser.parse_args())