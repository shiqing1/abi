import argparse
from pathlib import Path
import pandas as pd
import os
import shutil

parser = argparse.ArgumentParser()
parser.add_argument('-i','--map-csv-dir',type=Path,default=Path(r'D:\000-clobotics-workspace\002-data\002-data-intermediate-processing\00000000-tx-computer-have-uploaded-images\success_202009280946.csv'))
parser.add_argument('-d','--dest-image-dir',type=str,default=r'D:\000-clobotics-workspace\002-data\002-data-intermediate-processing\20200928-aug-batch2.5-can\aug-images-step4-should-upload_using_new_imageurl')
parser.add_argument('-s','--src-image-dir',type=str,default=r'D:\000-clobotics-workspace\002-data\002-data-intermediate-processing\20200928-aug-batch2.5-can\aug-images-step4-should-upload')

def rename_image(map_csv_dir,src_image_dir,dest_image_dir):
    df = pd.read_csv(map_csv_dir)
    img_name_list = os.listdir(src_image_dir)

    for img_name in img_name_list:
        src_img_path = os.path.join(src_image_dir,img_name)
        img_url = ''.join(df[df['ImgPath'].str.contains(img_name)]['ImgUrl'].values)
        new_img_name = img_url[img_url.rindex('/')+1:]+'.jpg'
        dest_img_path = os.path.join(dest_image_dir,new_img_name)
        shutil.copy(src_img_path,dest_img_path)

def main(args):
    return rename_image(args.map_csv_dir,args.src_image_dir,args.dest_image_dir)


if __name__ == '__main__':
    main(parser.parse_args())