
def _get_dir_path(dir,suffix):
    if dir.is_dir():
        return sorted(dir.rglob(suffix))
    return [dir]