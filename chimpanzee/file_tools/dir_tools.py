import os
import shutil

def check_dir(*dirs):

    for dir in dirs:
        if not os.path.exists(dir):
            os.mkdir(dir)
        else:
            shutil.rmtree(dir, True)
            os.mkdir(dir)