#!/usr/bin/env python
#-*- coding:utf-8 -*-
"""
replace item of csv,using mapping item name
Author:Zeng Haiyan
Date:20201009
"""
import pandas as pd
import argparse

parser = argparse.ArgumentParser()
parser.add_argument('-s','--src-csv-path',type=str,default=r'D:\000-clobotics-workspace\CV2020P08-ABI-TT\002-data\003-model-train-test-data\TT-channel\20200923-TestData\374204_ee7dbf2c-856d-48ad-95ad-ad15d1741ce9_result _delete-singlebox_maptott.csv')
parser.add_argument('-d','--dest-csv-path',type=str,default=r'D:\000-clobotics-workspace\CV2020P08-ABI-TT\002-data\003-model-train-test-data\TT-channel\20200923-TestData\374204_ee7dbf2c-856d-48ad-95ad-ad15d1741ce9_result _delete-singlebox_maptott_tosegmap.csv')
parser.add_argument('-m','--metadata-csv-path',type=str,default=r'D:\000-clobotics-workspace\CV2020P08-ABI-TT\002-data\000-metadata\Id_Name_metadata_20200921.csv')

def replace(src_csv_path,dest_csv_path,metadata_csv_path):
    #read metadata and get the mapping relationship
    df_metadata = pd.read_csv(metadata_csv_path)
    generic_box_mapping_dict = {}
    for index,row in df_metadata.iterrows():
        orignal_id = row['Id']
        tmp_row_type = row['type']
        if tmp_row_type == 'single box':
            generic_box_mapping_dict[orignal_id] = '1104881'
        if tmp_row_type == "half box":
            generic_box_mapping_dict[orignal_id] = '1083305'
    
    #replace product id using mapping productid
    df_src_csv = pd.read_csv(src_csv_path)
    for o_index,o_row in df_src_csv.iterrows():
        orignal_productid = int(o_row['ProductId'])
        if orignal_productid in generic_box_mapping_dict:
            print(orignal_productid)
            df_src_csv.replace(orignal_productid,generic_box_mapping_dict[orignal_productid],inplace=True)

    #save new csv
    df_src_csv.to_csv(dest_csv_path,index=False)

def main(args):
    replace(args.src_csv_path,args.dest_csv_path,args.metadata_csv_path)

if __name__ == '__main__':
    main(parser.parse_args())