"""
merge polygon to polygon
@Author:Zeng Haiyan
@Date:20200909
"""
import pandas as pd
import argparse

parser = argparse.ArgumentParser()
parser.add_argument('-s','--source-label-csv-path',type=str,default=r'D:\000-clobotics-workspace\CV2020P08-ABI-TT\002-data\002-data-intermediate-processing\00000002-create-task\20201106_10000_inference_labelling\csv\1023model_segment_10000data_1106_totalv100.csv')
parser.add_argument('-d','--destination-label-csv-path',type=str,default='')

def main(args):
    df = pd.read_csv(args.source_label_csv_path)

    group = df.groupby('ImageUrl')

    polygon_list = []
    productid_list = []
    imageurl_list = []
    for item,gr in group:
        imageurl_list.append(item)

        if len(gr['Polygon'].values.tolist()) > 0:
            polygons = []
            for polygon in gr['Polygon'].values.tolist():
                polygons.append(eval(polygon))
            polygon_list.append(polygons)

        if len(gr['ProductId'].values.tolist()) > 0:
            productids = []
            for productid in gr['ProductId'].values.tolist():
                productids.append(productid)
            productid_list.append(productids)

    df = pd.DataFrame({'ImageUrl':imageurl_list,'PolygonsList':polygon_list,'ProductIdList':productid_list})
    df.to_csv(r'D:\000-clobotics-workspace\CV2020P08-ABI-TT\002-data\002-data-intermediate-processing\00000002-create-task\20201106_10000_inference_labelling\csv\1023model_segment_10000data_1106_totalv100-task3000.csv',index=False)

if __name__ == '__main__':
    main(parser.parse_args())