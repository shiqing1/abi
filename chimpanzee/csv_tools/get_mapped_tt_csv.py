"""
replace csv with mapped id
@Date:20201016
@Author:zenghaiyan
"""
import pandas as pd
import argparse
from file_tools import  csv_file_tools
from pathlib import Path

parser = argparse.ArgumentParser()
parser.add_argument('-m','--mapping-csv-path',type=str,default=r'D:\000-clobotics-workspace\CV2020P08-ABI-TT\002-data\000-metadata\TT_mapping_file.csv')
parser.add_argument('-i','--input-csv-dir',type=Path,default=Path(r'D:\000-clobotics-workspace\CV2020P08-ABI-TT\002-data\003-model-train-test-data\TT-channel\TestResult\20200923-TestResult\batch1-TT-embedding025-add-bc-jz-t13p5-20201109\20201105_embedding025_batch1TT_add_singlebox_bingcui_jingcui_threshold13p5.csv'))
parser.add_argument('-o','--output-csv-path',type=str,default=r'D:\000-clobotics-workspace\CV2020P08-ABI-TT\002-data\003-model-train-test-data\TT-channel\TestResult\20200923-TestResult\batch1-TT-embedding025-add-bc-jz-t13p5-20201109\20201105_embedding025_batch1TT_add_singlebox_bingcui_jingcui_threshold13p5_maptott.csv')

def replace_csv_using_mapped_csv(mapping_csv_path,input_csv_dir,output_csv_path):

    #mapping id to abi tt product id
    mapping_df = pd.read_csv(mapping_csv_path)
    mapping_dict = dict(zip(mapping_df['origin_id'],mapping_df['mapped_to_id']))

    input_df = pd.concat([pd.read_csv(p) for p in csv_file_tools._get_dir_path(input_csv_dir,'.csv')])

    for origin_id in mapping_dict:
        mapping_id = mapping_dict[origin_id]
        input_df.replace(origin_id,mapping_id, inplace=True)

    #delete some product_id which we won't want
    # input_df = input_df[~(input_df['ProductId']==1104881)]
    input_df = input_df[~(input_df['ProductId']==1104882)]
    input_df = input_df[~(input_df['ProductId'] == 1083305)]
    input_df.to_csv(output_csv_path,index=False)

def main(args):
    replace_csv_using_mapped_csv(args.mapping_csv_path,args.input_csv_dir,args.output_csv_path)

if __name__ == '__main__':
    main(parser.parse_args())