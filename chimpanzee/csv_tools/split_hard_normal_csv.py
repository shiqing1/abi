import os
from csv_tools import select_rows_in_csv
from pathlib import Path

#来了新的表格，一般是需要修改两个参数，就是目标的csv和原始csv

#将gt的标注csv进行拆分。也可以将预测的结果csv进行拆分,需要拆分之后才能够计算每一种的准确率
hard_dir = r'D:\000-clobotics-workspace\CV2020P08-ABI-TT\002-data\003-model-train-test-data\TT-channel\20200923-TestData\images_split_hard_normal\hard'
origination_csv = Path(r'D:\000-clobotics-workspace\CV2020P08-ABI-TT\002-data\003-model-train-test-data\TT-channel\20200923-TestResult\batch1-TT-embedding-model-20201020\predict_csv\20201020-1009-batch1-tt-embedding.csv')
destination_csv_dir =r'D:\000-clobotics-workspace\CV2020P08-ABI-TT\002-data\003-model-train-test-data\TT-channel\20200923-TestResult\batch1-TT-embedding-model-20201020\predict_csv\maptt-split-csv'

#质量不好的那一堆的csv生成
dirs = os.listdir(hard_dir)
for dir in dirs:
    tmp_dir = os.path.join(hard_dir, dir)
    destination_csv_path = os.path.join(destination_csv_dir, dir +'.csv')
    select_rows_in_csv.select_rows_record(tmp_dir, 'ImageUrl', '', origination_csv, destination_csv_path)

#正常的图像的csv生成，图像的路径可以不经常进行改动
normal_dir = r'D:\000-clobotics-workspace\CV2020P08-ABI-TT\002-data\003-model-train-test-data\TT-channel\20200923-TestData\images_split_hard_normal\normal'
normal_destination_csv_path = os.path.join(destination_csv_dir,'normal.csv')
select_rows_in_csv.select_rows_record(normal_dir,'ImageUrl','',origination_csv,normal_destination_csv_path)