import numpy as np

def get_str_polygon_to_array(polygon):
    """
    get array of str type array
    :param polygon:
    :return:
    """
    if type(polygon)==str:
        polygon_str_list = polygon.replace('[', '').replace(']', '').split(',')
        polygon_number_list = list(map(float, polygon_str_list))
        polygon_arr = np.array(polygon_number_list)
        polygon_arr = np.reshape(polygon_arr, (-1, 2))
        polygon_arr[:, 0] = polygon_arr[:, 0]
        polygon_arr[:, 1] = polygon_arr[:, 1]

        return polygon_arr
    return polygon