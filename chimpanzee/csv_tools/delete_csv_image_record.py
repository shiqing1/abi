#! /usr/lib/env python
#-*- coding=utf-8 -*-
"""
delete the record of images
删除图像对应的记录，挑选出了不合格的图像，需要从记录中把它删除掉
Author:Zeng Haiyan
Date:20200902
"""
import os
from pathlib import Path
import pandas as pd
import argparse

parser = argparse.ArgumentParser()
parser.add_argument('-i','--img-dir',type=str,default=r'D:\000-clobotics-workspace\ignore_images')
parser.add_argument('-c','--csv-dir',type=Path,default=Path(r'D:\000-clobotics-workspace\002-data\002-data-intermediate-processing\20200922-aug-batch12'))
parser.add_argument('-d','--destination-csv-path',type=str,default=r'D:\000-clobotics-workspace\002-data\002-data-intermediate-processing\20200922-total-aug-batch12.csv')
parser.add_argument('-v','--is-csv',type=bool,default=True)
parser.add_argument('-e','--delete-img-url-csv-path',type=str,default=r'D:\000-clobotics-workspace\002-data\002-data-intermediate-processing\20200922-aug-batch12-should-delete-img-record\batch12-should-delete.csv')

def remove_bad_img_records(df, bad_img_paths):
    df['img_md5'] = df.ImageUrl.str.split("/").str[-1].str.strip(".jpg")
    bad_img_names = [p.stem for p in bad_img_paths]
    return df[~df.img_md5.isin(bad_img_names)]

def delete_img():
    dir_path_list = [r'D:\000-扩博智能-工作目录\1-项目相关\2020-P-TT堆箱-百威啤酒\000-数据\百威堆箱训练集20200516_20200522_人工review结果-Rongyi\cut_box_to_label',
                r'D:\000-扩博智能-工作目录\1-项目相关\2020-P-TT堆箱-百威啤酒\000-数据\百威堆箱训练集20200516_20200522_人工review结果-Rongyi\labeling_errors',
                r'D:\000-扩博智能-工作目录\1-项目相关\2020-P-TT堆箱-百威啤酒\000-数据\百威堆箱训练集20200516_20200522_人工review结果-Rongyi\rotation',
                r'D:\000-扩博智能-工作目录\1-项目相关\2020-P-TT堆箱-百威啤酒\000-数据\割箱标注问题数据 - 副本',
                r'D:\000-扩博智能-工作目录\1-项目相关\2020-P-TT堆箱-百威啤酒\000-数据\其他标注有问题数据 - 副本',]

    count = 0

    data_0516 = pd.read_csv('20200516.csv')
    data_0522 = pd.read_csv('exported-train-20200522.csv')

    #assert len(set(data_0516.ImageUrl.tolist()) & set(data_0522.ImageUrl.tolist())) == 0

    #bad_img_paths = [j for d in dir_path_list for j in Path(d).glob("*.jpg")]
    a = 0
    for dir_path in dir_path_list:
        # dir_path = Path(dir_path)
        tmp_img_name_list = os.listdir(dir_path)

        for img_name in tmp_img_name_list:
            print(img_name)
            count += 1
            print(count)

            tmp_str = img_name[:img_name.index('.')]
            print(tmp_str)

            data_0516 = data_0516[~ data_0516['ImageUrl'].str.contains(tmp_str)]
            data_0522 = data_0522[~ data_0522['ImageUrl'].str.contains(tmp_str)]

    data_0516.to_csv('20200516_delete_disqualification.csv')
    data_0522.to_csv('20200522_delete_disqualification.csv')

def get_img_name_in_csv(csv_path):
    df = pd.read_csv(csv_path)
    img_url_list = df['ImgUrl'].values.tolist()
    img_name_list = [img_url[img_url.rindex('/')+1:]+'.jpg' for img_url in img_url_list]
    return img_name_list

def _get_file_path(path):
    """
    get file path list of dir
    :param path:
    :return:
    """
    if path.is_dir():
        return sorted(path.rglob("*.csv"))
    return [path]

def delete_image_record(csv_dir,img_dir,destination_csv_path,is_csv,delete_img_url_csv_path):
    """
    delete img record from csv according to the image names
    :param csv_csv_dir:
    :param img_dir:
    :param destination_csv_path:
    :return:
    """
    df = pd.concat([pd.read_csv(p) for p in _get_file_path(csv_dir)])

    print(len(df['ImageUrl'].unique()))
    if is_csv:
        img_name_list = get_img_name_in_csv(delete_img_url_csv_path)
    else:
        img_name_list = os.listdir(img_dir)

    for img_name in img_name_list:
        img_md5 = img_name[:img_name.index('.')]
        df = df[~ df['ImageUrl'].str.contains(img_md5)]

    df.to_csv(destination_csv_path,index=False)

def main(args):
    delete_image_record(args.csv_dir,args.img_dir,args.destination_csv_path,args.is_csv,args.delete_img_url_csv_path)

if __name__ == '__main__':
    main(parser.parse_args())