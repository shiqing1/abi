"""
generate new csv file according to the item
@Author:Zeng Haiyan
@Date:20200908
"""
import argparse
import pandas as pd
import os
from pathlib import Path
from file_tools import csv_file_tools

parser = argparse.ArgumentParser()
parser.add_argument('-i','--names-dir',type=str,default=None)
parser.add_argument('-s','--spare-tire-csv-path',type=str,default=r'../data/20200908_select_can_beer.csv')
parser.add_argument('-r','--origination-csv-dir',type=Path,default=Path(r'D:\000-clobotics-workspace\002-data\20200908-select-generic-box-label-task'))
parser.add_argument('-o','--destination-csv-path',type=str,default='../data/20200908-select-can-beer-record.csv')
parser.add_argument('-n','--column-name',type=str,default='ImageUrl')

def select_rows_record(names_dir,column_name, spare_tire_csv_path,origination_csv_dir, destination_csv_path):
    """
    select rows to a new csv file
    :param names_dir:
    :param spare_tire_csv_path:
    :param img_url_str:
    :param destination_csv_path:
    :return:
    """
    #get select set items
    if names_dir != None:
        spare_list = [jpg_name[:jpg_name.index('.jpg')] for jpg_name in os.listdir(names_dir)]
    else:
        spare_list = pd.read_csv(spare_tire_csv_path)[column_name].values.tolist()

    #conduct csv file
    destination_df = pd.DataFrame()
    original_df = pd.concat([pd.read_csv(p) for p in csv_file_tools._get_dir_path(origination_csv_dir,'*.csv')])
    for item in spare_list:
        select_item = original_df[original_df[column_name].str.contains(item)]
        if len(select_item)>0:
            destination_df = destination_df.append(select_item)
        else:
            print(item)
    destination_df.to_csv(destination_csv_path,index=False)

def main(args):
    select_rows_record(args.names_dir, args.column_name, args.spare_tire_csv_path, args.origination_csv_dir, args.destination_csv_path)

if __name__ == '__main__':
    main(parser.parse_args())