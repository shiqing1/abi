#!/usr/bin/env python
# -*- coding: utf-8 -*-
#@Time  : 2020/5/25 下午8:26
#@Author: Xing Tong
#@Email : xing.tong@clobotics.com
"""
This is used for ...
"""
import os
import argparse
import glob
import datetime

import pandas as pd
import numpy as np


def tran_per_df(src_df):
    """Transform a labme like csv to a labqa like csv"""
    new_rows = []
    for i, row in src_df.iterrows():
        url = row['ImageUrl']
        polygon = eval(row['Polygon'])
        xmin, ymin = np.min(polygon, axis=0)
        xmax, ymax = np.max(polygon, axis=0)
        new_rows.append([url, row['ProductId'], xmin, ymin, xmax, ymax, row['TaskItemId'], row['Rotation'], row['TaskId']])
    new_df = pd.DataFrame(data=new_rows,
                          columns=['ImgUrl', 'ProductId', 'xmin', 'ymin', 'xmax', 'ymax', 'TaskItemId', 'Rotation',
                                   'TaskId'])
    return new_df


def labme_2_labqa(df):
    """Transforma csvs with format like labme in src_dir to those like labqa, and save to dst_dir

    Args:
      src_dir: str, a path containing csv files of labme format.
      dst_dir: str, a path to save csv files like labqa format.

    Returns: None
    """
    before_len = len(df)
    df = df[df["Classifications"] == "[]"]
    after_len = len(df)
    print("before length: {}, after length: {}".format(before_len, after_len))
    new_df = tran_per_df(df)
    new_df['IsMaskQA'] = 0
    new_df['ImageQuality'] = "[]"
    new_df['SceneType'] = 3
    new_df['RequestId'] = ''

    return new_df


if __name__ == '__main__':

    parser = argparse.ArgumentParser(description='Crop images using exported labqa csv files.')

    parser.add_argument(
        "-id", "--input_dir", type=str,default=r'D:\000-clobotics-workspace\CV2020P07-ABI-CR\Sku\000_test_set\169185_origin',
        help='The input directory containing csv files exported from labme')
    parser.add_argument(
        "-od", "--output_dir", type=str,default=r'D:\000-clobotics-workspace\CV2020P07-ABI-CR\Sku\000_test_set\169185_labelqa_format',
        help="The output directory to save csv for training.")

    args = parser.parse_args()

    csv_file_list = glob.glob(os.path.join(args.input_dir, '*.csv'))

    for csv_file in csv_file_list:

        #taskid = csv_file.split('/')[-1].split('_')[0]

        df = pd.read_csv(csv_file)

        df = df[df.ProductId != 1265]

        df = labme_2_labqa(df)

        # df.head()
        df.to_csv(os.path.join(args.output_dir, 'abi_sku_{}_{}.csv'.format('169185', datetime.datetime.strftime(datetime.datetime.now(), "%Y%m%d"))), index=False)
