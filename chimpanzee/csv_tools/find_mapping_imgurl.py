"""
find mapping image urls on labman according to local images dir
@Author:Zeng Haiyan
@Date:20200908
"""
import argparse
import pandas as pd
from pathlib import Path
import os
from file_tools import csv_file_tools

parser = argparse.ArgumentParser()
parser.add_argument('-m','--mapping-csv-dir',type=Path,default=Path(r'D:\000-clobotics-workspace\002-data\上传过的图像和本地图像对应关系'))
parser.add_argument('-i','--image-dir',type=str,default=r'D:\000-clobotics-workspace\002-data\20200908-挑选需要重新标注的纸盒子-听装盒子装酒')
parser.add_argument('-s','--source-column',type=str,default='ImgPath')
parser.add_argument('-d','--destination-column',type=str,default='ImgUrl')
parser.add_argument('-a','--destination-img-url-csv-path',type=str,default='../data/20200908_select_can_beer.csv')


def main(args):

    mapping_df = pd.concat([pd.read_csv(p,encoding='utf-8') for p in csv_file_tools._get_csv_dir_path(args.mapping_csv_dir)])
    img_name_list = [name[:name.rindex('.')]for name in os.listdir(args.image_dir)]

    img_url_list = []
    for img_name in img_name_list:
        tmp_url = mapping_df[mapping_df['ImgPath'].str.contains(str(img_name))].ImgUrl.values.tolist()
        if len(tmp_url) > 0:
            img_url_list.append(tmp_url[0])
        else:
            print(img_name)

    img_url_df = pd.DataFrame({'ImgUrl':img_url_list})
    img_url_df.to_csv(args.destination_img_url_csv_path,index=False)

if __name__ == '__main__':
    main(parser.parse_args())