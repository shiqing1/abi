#!/usr/bin/env python
#-*- coding:utf-8 -*-
"""
replace item of csv files according to the mapping files
Author:Zeng Haiyan
Date:20200901
"""
import pandas as pd
import argparse
from pathlib import Path

parser = argparse.ArgumentParser()
parser.add_argument('-i','--input-csv-path',type=Path,default=Path(r'D:\000-clobotics-workspace\CV2020P08-ABI-TT\002-data\002-data-intermediate-processing\20201009-TT-traindata\376352_csv_process_step3\376352_delete_confusion_area_mapping_productid.csv'))
parser.add_argument('-o','--output-csv-path',type=str,default=r'D:\000-clobotics-workspace\CV2020P08-ABI-TT\002-data\002-data-intermediate-processing\20201009-TT-traindata\376352_csv_process_step3\376352_delete_confusion_area_mapping_productid_replace_gray_imageurl.csv')
parser.add_argument('-m','--mapping-csv-path',type=str,default=r'D:\000-clobotics-workspace\CV2020P08-ABI-TT\002-data\002-data-intermediate-processing\20201009-TT-traindata\376352-gray-image-upload-imgurl-mapping-csv-step1\success_202010091529.csv')
parser.add_argument('-s','--suffix',type=str,default=r'_gray')

def main(args):
    replace_imgurl(args.input_csv_path, args.step5_mapping_csv_path, args.suffix, args.output_csv_path)

def replace_imgurl(input_csv_path,mapping_csv_path,suffix,output_csv_path):
    # read batch csv file from directory
    df = pd.concat([pd.read_csv(p) for p in _get_file_path(input_csv_path)])
    #read mapping csv
    map_df = pd.read_csv(mapping_csv_path)

    #replace item
    for index,row in map_df.iterrows():
        origin_img_path = row['ImgPath']
        img_path_suffix = origin_img_path[origin_img_path.rindex('\\'):]
        if suffix in img_path_suffix:
            fileman_prefix = 'https://fileman.clobotics.cn/api/file/'
            origin_img_url = fileman_prefix + origin_img_path[origin_img_path.rindex('\\')+1:origin_img_path.index(suffix)]
            df.replace(origin_img_url, row['ImgUrl'], inplace=True)

    #save new file
    df.to_csv(output_csv_path,index=False)

def _get_file_path(path):
    """
    get file path list of dir
    :param path:
    :return:
    """
    if path.is_dir():
        return sorted(path.rglob("*.csv"))
    return [path]


if __name__ == '__main__':
    main(parser.parse_args())