#! /usr/lib/env python
#-*- coding=utf-8 -*-
"""
delete the record of one column
Author:Zeng Haiyan
Date:20201009
"""
import os
from pathlib import Path
import pandas as pd
import argparse

parser = argparse.ArgumentParser()
parser.add_argument('-c','--csv-dir',type=Path,default=Path(r'D:\000-clobotics-workspace\CV2020P08-ABI-TT\002-data\002-data-intermediate-processing\20201009-TT-traindata\376352_delete_confusion_area.csv'))
parser.add_argument('-d','--destination-csv-path',type=str,default=r'D:\000-clobotics-workspace\CV2020P08-ABI-TT\002-data\002-data-intermediate-processing\20201009-TT-traindata\376352_delete_confusion_area.csv')
parser.add_argument('-v','--colname',type=str)
parser.add_argument('-e','--delete-value',type=str)

def _get_file_path(path):
    """
    get file path list of dir
    :param path:
    :return:
    """
    if path.is_dir():
        return sorted(path.rglob("*.csv"))
    return [path]

def delete_record(csv_dir, destination_csv_path, colname='ProductId', delete_value='1'):

    df = pd.concat([pd.read_csv(p) for p in _get_file_path(csv_dir)])

    df = df[~ df[colname].isin([delete_value])]

    df.to_csv(destination_csv_path,index=False)

def main(args):
    delete_record(args.csv_dir, args.destination_csv_path)

if __name__ == '__main__':
    main(parser.parse_args())