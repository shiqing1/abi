/* Defines the schema for communication with MlMan.
   This schema can also be used by SomeMan to communicate with 
   AlgoMan as AlgoMan is a gateway to MlMan
*/
syntax = "proto3";

package mlman;

import "internal/common/common.proto";
import "internal/common/common_retail.proto";


message ObjectDetectionRequest {
    enum Mode {
        UNKNOWN_MODE = 0;
        UNIT_ONLY = 1;     // only detect units
        SKU_ONLY = 2;      // detect sku from a whole image
        UNIT_AND_SKU = 3;  // detect units and what each unit is
    }
    Mode mode = 1;

    // Needed if it is SKU_ONLY.
    repeated common.BoundingBox rec = 2;
}


message ObjectDetectionResult {
    // One image to be processed
    common.ImageInfo image_id = 1;

    repeated common.BoundingBoxObject box = 2;

    // Detected orientation of an image measured in degree clock wise.
    // By rotating the image this degree counterclock wise, we can get the image
    // look normal.
    float detected_orientation = 3;

    // If the image should be sent for QA
    bool should_qa = 4;
}


message PolygonDetectionResult {
    // One image to be processed
    common.ImageInfo image_id = 1;

    repeated common.PolygonObject polygon = 2;

    // If the polygons should be sent for QA
    bool should_qa = 3;
}


message ObjectDetectionResponse {
    common.ResponseInfo common_response_info = 1;

    ObjectDetectionResult result = 2;

    PolygonDetectionResult polygon_result = 3;

}

message SceneClassificationResult {
    common.ImageInfo image_id = 1;

    // detected scenes; could be multiple
    repeated common.retail.Scene.SceneType scene = 2;
}

message SceneClassificationResponse {
    common.ResponseInfo common_response_info = 1;

    SceneClassificationResult result = 2;
}

message ImageQualityAssessmentResult {
    common.ImageInfo image_id = 1;

    repeated common.retail.ImageQuality.ImageQualityType image_quality = 2;
}

message ImageQualityAssessmentResponse {
    common.ResponseInfo common_response_info = 1;

    ImageQualityAssessmentResult result = 2;
}

message ImageRecognitionResponse {
    ObjectDetectionResponse object_detection_response = 2;
    SceneClassificationResponse scene_classification_response = 3;
    ImageQualityAssessmentResponse image_quality_response = 4;
}