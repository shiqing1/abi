@echo off
REM Script to compile proto files for C#
REM Usage: compile_proto.cmd <PATH_TO_PROTO_COMPILER> [language]
REM        language is optional; by default it will output csharp files
REM	   current support csharp, python
REM Example: compile_proto.bat c:\tools\protoc-3.5.1-win32\bin

setlocal enabledelayedexpansion

if "%1"=="" (set COMPILER_DIR=c:\tools\protoc-3.5.1-win32\bin) else (set COMPILER_DIR=%1)
set LANGUAGE=%2

set OUTPUT_TYPE=csharp_out
if "%LANGUAGE%"=="python" set OUTPUT_TYPE=python_out
set PYTHON_DES_DIR=%~dp0
set PYTHON_DES_DIR=!PYTHON_DES_DIR:Protobuf=Protobuf!
REM echo PYTHON_DES_DIR: !PYTHON_DES_DIR!

set COMPILER=%COMPILER_DIR%\protoc.exe
echo COMPILER: %COMPILER%

echo "compile *.proto files and generate *.cs or *.py files"
for /r %%f in (*.proto) do (
    echo proto file: %%f
    set DES_DIR=%%~dpf
    set DES_DIR=!DES_DIR:Protobuf=Protobuf!
    echo DES_DIR: !DES_DIR!
    if not exist !DES_DIR! md !DES_DIR!
    if "!OUTPUT_TYPE!"=="python_out" (
	%COMPILER% -I=%~dp0 --!OUTPUT_TYPE!=!PYTHON_DES_DIR! %%f
    ) else (
	%COMPILER% -I=%~dp0 --!OUTPUT_TYPE!=!DES_DIR! %%f
    )
    copy %%f !DES_DIR!
)
pause