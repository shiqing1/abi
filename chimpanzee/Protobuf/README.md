# Protocol Buffer
The procedure to use protobuf for is three main steps

* Define message formats in a .proto file.
* Use the protocol buffer compiler protoc to generate language-specific classes (eg. Python classes, Java classes etc).
* Use the language-specific protocol buffer API to write and read messages.

## Installation

You would need at least two things to be installed, a protocol buffer compiler (protoc) and a protobuf library.

### Ubuntu 16.04

* Install protobuf compiler and protobuf using the following command.   
```
cd ~
wget -nc https://github.com/google/protobuf/releases/download/v3.5.1/protoc-3.5.1-linux-x86_64.zip -P ~/
unzip protoc-3.5.1-linux-x86_64.zip -d protoc3
sudo mv protoc3/bin/* /usr/local/bin/
sudo mv protoc3/include/* /usr/local/include/
```

### Windows

* Go to https://github.com/google/protobuf/releases and download the zip file containg the latest compiler (v3.5.1 as of the writing)
* Unzip it to some folder, say, C:\Tools\protoc-3.5.1-win32

## Defining your Protocol Format
The definitions in a .proto file are simple: you add a message (eg. class) for each data structure you want to serialize, then specify a name and a type for each field in the message. Here is the .proto file that defines the MLMan's API interface, [mlman.proto](services/mlman/mlman.proto).

Right now we use proto2 syntax. The full language guide can be found [here](https://developers.google.com/protocol-buffers/docs/proto), which describes how to define a .proto file.

## Compiling your protocol buffers
Now that you have a .proto, the next thing you need to do is generate the classes you'll need to read and write the proto (and hence input and output specification) messages. To do this, you need to run the protocol buffer compiler protoc on your .proto:

Now run the compiler, specifying the source directory (where your application's source code lives – the current directory is used if you don't provide a value), the destination directory (where you want the generated code to go; often the same as $SRC_DIR), and the path to your .proto. In this case, you...:

        protoc --python_out=$DST_DIR $SRC_DIR/services/mlman/mlman.proto

For eg, if you want Python classes, you use the --python\_out option.
This generates mlman_pb2.py in your specified destination directory.

To build all the .proto files in this project, you can directly run the [compile_proto.sh](compile_proto.sh)

```
bash compile_proto.sh ALL
```

In Windows, you can compile the .proto files by running the following in a command window:
```
compile_proto.cmd <path_to_proto_compiler>\bin python
```


## Usage of Protocol Buffer API
Each of the language has different API generated for the protocol buffer.

For eg, for python, you can refer to https://developers.google.com/protocol-buffers/docs/pythontutorial.

## Using Protobuf in other git repositories.

### One-time setup
To add Protobuf as a submodule in another git repo.

* Go to your favorite git repo, eg. caffe.

* git add submodule this repo.

```
git submodule add https://gitlab.com/Clobotics/Protobuf.git Protobuf
git commit
```

Once this is done, the submodule up to the particular commit id above is added into your git repo.

### Fetching the submodule
If the submodule is already added in your git repo, you can do this to fetch the version that is committed into your repo.
Note that this is not necessarily the latest commit in the original submodule project.
See the next section on how to update the submodule.

* If others have added the submodule for your repo or the submodule is rolled forward into a new commit id, then do.

```
git submodule init
git submodule update
```

* For cloning a new repo with submodule, you can also do the following to get all the submodule:

```
git clone --recursive <url>
```

### Updating the submodule
If the submodule has new updates that you would like to pull into your git repo, you have the update the commit id associated with this submodule.

* To update the submodule to the new commit, just do git pull in the submodule directory and then do git commit.

```
# In the submodule directory, do:
git pull
git add $SUBMODULE
git commit -m 'Updated submodule to the latest commit.'
```

* Note that sometimes you might not be able to pull anything. In that case, do the following:

```
git status
# It might be saying that you are in a detached state. Do the following.
git checkout master
git pull
```

If you do git diff before git add, you will see that the change is merely a new commit id associated with the submodule.
