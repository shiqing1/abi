# TODO: We should probably move to Makefile?

#!/bin/bash

if [ "$#" -lt 1 ]; then
  echo "bash compile_proto.sh [LANGUAGE]"
  echo "LANGUAGE is either ALL, python, c++, java etc"
  exit
fi

SRC_DIR=.
DES_DIR=.

LANGUAGE=$1

if [ "$LANGUAGE" == 'python' ] || [ "$LANGUAGE" == 'ALL' ]; then
  protoc --proto_path=$SRC_DIR --python_out=$DES_DIR $SRC_DIR/internal/common/*.proto
  protoc --proto_path=$SRC_DIR --python_out=$DES_DIR $SRC_DIR/internal/services/*.proto
  protoc --proto_path=$SRC_DIR --python_out=$DES_DIR $SRC_DIR/internal/services/mlman/*.proto
  protoc --proto_path=$SRC_DIR --python_out=$DES_DIR $SRC_DIR/internal/services/cvman/*.proto
  protoc --proto_path=$SRC_DIR --python_out=$DES_DIR $SRC_DIR/internal/services/algoman/*.proto
  protoc --proto_path=$SRC_DIR --python_out=$DES_DIR $SRC_DIR/internal/services/bizman/*.proto
fi

if [ "$LANGUAGE" == 'swift' ] || [ "$LANGUAGE" == 'ALL' ]; then
  protoc --proto_path=$SRC_DIR --swift_out=$DES_DIR $SRC_DIR/external/*.proto
  protoc --proto_path=$SRC_DIR --swift_out=$DES_DIR $SRC_DIR/external/retail/*.proto
  protoc --proto_path=$SRC_DIR --swift_out=$DES_DIR $SRC_DIR/internal/common/*.proto
  protoc --proto_path=$SRC_DIR --swift_out=$DES_DIR $SRC_DIR/internal/services/clientman/*.proto
  protoc --proto_path=$SRC_DIR --swift_out=$DES_DIR $SRC_DIR/internal/services/bizman/*.proto
fi
