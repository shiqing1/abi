'''
date:2021-01-01
author:shiqing
'''

from shiqingTools import  tools 
import argparse 
import glob 

'''
merge csv files under a folder 
'''

parser = argparse.ArgumentParser()
parser.add_argument('-d','--dir',required=True)
parser.add_argument('-o','--output',required=True)
args = parser.parse_args()

def main():
    files = glob.glob(args.dir+'/*.csv')
    tools.merge_csv(args.output,*files)


if __name__ == "__main__":
    main()