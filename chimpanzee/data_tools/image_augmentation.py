from skimage import io
import imgaug.augmenters as iaa
import argparse
import numpy as np
import imgaug as ia
import pandas as pd
from pathlib import Path
import os
from image_tools import download_image
from file_tools import csv_file_tools
from csv_tools import str_to_array
import albumentations as albu
import random
import tqdm

parser = argparse.ArgumentParser()
parser.add_argument('-i','--csv-dir',type=Path,default=Path('/home/zyl/workspace/ABI/group_data/20201231/csv/20201231_group_train.csv'))
parser.add_argument('-d','--dest-image-dir',type=str,default='/home/zyl/workspace/ABI/group_data/20201231/augmentation_images')
parser.add_argument('-s','--dest-csv-dir',type=str,default=r'/home/zyl/workspace/ABI/group_data/20201231/augmentation_csv')

def is_random_get(p):
    rand = random.randint(1, 100)
    if rand <= p*100:
        return True

def aug(img, aug_func):
    return aug_func(**{'image':img})['image']

def transpose(image):
    return image.transpose(1, 0, 2) if len(image.shape) > 2 else image.transpose(1, 0)

def image_polygon_transpose(image,polygon_arrs):
    #get the image shape
    (img_height,img_width,channel) = np.shape(image)

    #transpose pylogon labeled by labelman csv
    polygons_T = []
    for (index,polygon) in enumerate(polygon_arrs):
        tmp_point = []
        for polygon_point in str_to_array.get_str_polygon_to_array(polygon):
            tmp_point.append([polygon_point[1],polygon_point[0]])
        polygons_T.append(tmp_point)

    #transpose image
    img_T = transpose(image)
    return img_T,polygons_T

def image_augmentation(image,aug_func,dest_image_dir, file_name,img_suffix,p):
    """
    the transform would not cause to the polygon change
    :param image:
    :param aug_func:
    :param dest_image_dir:
    :param file_name:
    :param p: possibility
    :return:
    """
    if is_random_get(p):
        img_aug = aug(image.copy(), aug_func)
        new_image_path = os.path.join(dest_image_dir, file_name + '_' + img_suffix + '.jpg')
        io.imsave(new_image_path, img_aug)
        return True
    return False

def polygon_and_image_augmentation(image, polygon_arrs, origin_product_id_list,aug_func, dest_image_dir, file_name, img_suffix,p):
    """
    the transform would cause to the polygon change
    :param image:
    :param polygon_arrs:
    :param origin_product_id_list:
    :param aug_func:
    :param dest_image_dir:
    :param file_name:
    :param img_suffix:we can know the transformer type according to the suffix
    :param p: the change would not occur maybe
    :return:
    """
    if is_random_get(p):
        #get the image shape
        (img_height,img_width,channel) = np.shape(image)

        #pylogon labeled by labelman csv to imgaug polygon.we should check whether they can be polygon
        polygons = []
        product_ids = []
        for (index,polygon) in enumerate(polygon_arrs):
            tmp_point = []
            for polygon_point in str_to_array.get_str_polygon_to_array(polygon):
                tmp_point.append((polygon_point[0]*img_width,polygon_point[1]*img_height))
            if len(set(tmp_point)) > 2:
                polygons.append(ia.Polygon(tmp_point))
                product_ids.append(origin_product_id_list[index])

        #execute transform
        images_aug, polygons_aug = aug_func(image=image, polygons=polygons)

        # save new image
        new_image_path = os.path.join(dest_image_dir, file_name + '_' + img_suffix + '.jpg')
        io.imsave(new_image_path, images_aug)

        new_img_width,new_img_height = np.shape(images_aug)[:2]

        return polygons_aug,product_ids,new_img_width,new_img_height

    return None,None,None,None

def check_dir(*dirs):
    for dir in dirs:
        if not os.path.exists(dir):
            os.mkdir(dir)

def ABI_TT_image_augmentation(csv_dir,dest_image_dir,dest_csv_dir):

    #check dir
    check_dir(dest_image_dir,dest_csv_dir)

    #read labman labeled csv file
    df = pd.concat([pd.read_csv(p) for p in csv_file_tools._get_dir_path(csv_dir,'*.csv')])
    #group the csv by ImageUrl
    group_df = df.groupby('ImageUrl')

    #after transform the image, we should save the new polygons
    perspective_transform_polygons_list = []
    perspective_transform_imgurl_list = []
    perspective_transform_prodcuctid_list = []

    rotate_90_polygons_list = []
    rotate_90_imgurl_list = []
    rotate_90_prodcuctid_list = []

    rotate_180_polygons_list = []
    rotate_180_imgurl_list = []
    rotate_180_prodcuctid_list = []

    rotate_270_polygons_list = []
    rotate_270_imgurl_list = []
    rotate_270_prodcuctid_list = []

    flip_polygons_list = []
    flip_imgurl_list = []
    flip_prodcuctid_list = []

    GaussianBlur_df = pd.DataFrame()
    IAASharpen_df = pd.DataFrame()
    HueSaturationValue_df = pd.DataFrame()
    RandomBrightness_df = pd.DataFrame()
    RandomContrast_df = pd.DataFrame()
    GaussNoise_df = pd.DataFrame()
    for img_url,item in tqdm.tqdm(group_df):
        #get the image
        if '.jpg' in img_url:
            file_name = img_url[img_url.rindex('/') + 1:img_url.rindex('.')]
        else:
            file_name = img_url[img_url.rindex('/')+1:len(img_url)]
        img_path = os.path.join(dest_image_dir,file_name+'.jpg')
        if not os.path.exists(img_path):
            download_image.get_image(dest_image_dir,img_url,file_name)
        img = io.imread(img_path)

        #get the polygon array
        polygon_arrs = item['Polygon'].values
        product_id = item['ProductId'].values



        ##############################polygon and image augmentation##############################
        # #perspective transform
    
        # perspective_transform_polygons_aug,aug_perspective_transform_productid_list,img_width,img_heigt= polygon_and_image_augmentation(img,polygon_arrs,product_id,iaa.PerspectiveTransform(scale=(0.01, 0.15),keep_size=False),dest_image_dir,file_name,'perspective',1)
        # perspective_transform_polygons_list.extend([[[point[0]/img_heigt,point[1]/img_width] for point in polygon] for polygon in perspective_transform_polygons_aug])
        # #perspective_transform_imgurl_list.extend([img_url for i in range(len(perspective_transform_polygons_aug))])
        # perspective_transform_imgurl_list.extend([file_name+'_perspective.jpg'  for i in range(len(perspective_transform_polygons_aug))])
        # perspective_transform_prodcuctid_list.extend(aug_perspective_transform_productid_list)
        
        # # #rotate 90,inverse clock
        # img_T,polygon_T = image_polygon_transpose(img,polygon_arrs)
        # rotate_90_polygons_aug,rotate_90_product_id_list,img_width,img_heigt= polygon_and_image_augmentation(img_T,polygon_T,product_id,iaa.Flipud(1),dest_image_dir,file_name,'rotate90',0.1)
        # if rotate_90_polygons_aug != None:
        #     rotate_90_polygons_list.extend([[[point[0] /img_heigt, point[1] / img_width] for point in polygon] for polygon in rotate_90_polygons_aug])
        #     #rotate_90_imgurl_list.extend([img_url for i in range(len(rotate_90_polygons_aug))])
        #     rotate_90_imgurl_list.extend([file_name+'_rotate90.jpg'  for i in range(len(rotate_90_polygons_aug))])
        #     rotate_90_prodcuctid_list.extend(rotate_90_product_id_list)

        # # rotate 180,inverse clock
        # rotate_180_polygons_aug,rotate_180_product_id_list,img_width,img_heigt = polygon_and_image_augmentation(img,polygon_arrs,product_id,iaa.Sequential([iaa.Fliplr(1),iaa.Flipud(1)]),dest_image_dir,file_name,'rotate180',0.1)
        # if rotate_180_polygons_aug != None:
        #     rotate_180_polygons_list.extend([[[point[0] / img_heigt, point[1] / img_width] for point in polygon] for polygon in rotate_180_polygons_aug])
        #     #rotate_180_imgurl_list.extend([img_url for i in range(len(rotate_180_polygons_aug))])
        #     rotate_180_imgurl_list.extend([file_name+'_rotate180.jpg'  for i in range(len(rotate_180_polygons_aug))])
        #     rotate_180_prodcuctid_list.extend(rotate_180_product_id_list)

        # # rotate 270,inverse clock
        # rotate_270_polygons_aug,rotate_270_product_id_list,img_width,img_heigt = polygon_and_image_augmentation(img_T, polygon_T,product_id,iaa.Fliplr(),dest_image_dir, file_name,'rotate270',0.1)
        # if rotate_270_polygons_aug != None:
        #     rotate_270_polygons_list.extend([[[point[0] / img_heigt, point[1] / img_width] for point in polygon] for polygon in rotate_270_polygons_aug])
        #     #rotate_270_imgurl_list.extend([img_url for i in range(len(rotate_270_polygons_aug))])
        #     rotate_270_imgurl_list.extend([file_name+'_rotate270.jpg'  for i in range(len(rotate_270_polygons_aug))])
        #     rotate_270_prodcuctid_list.extend(rotate_270_product_id_list)
    
        #mirror flip
        flip_polygons_aug,aug_flip_polygons_productid_list,img_width,img_heigt = polygon_and_image_augmentation(img,polygon_arrs,product_id,iaa.Fliplr(),dest_image_dir,file_name,'mirror_flip',1)
        flip_polygons_list.extend([[[point[0] / img_heigt, point[1] / img_width] for point in polygon] for polygon in flip_polygons_aug])
        #flip_imgurl_list.extend([img_url for i in range(len(flip_polygons_aug))])
        flip_imgurl_list.extend([file_name+'_mirror_flip.jpg'  for i in range(len(flip_polygons_aug))])
        flip_prodcuctid_list.extend(aug_flip_polygons_productid_list)

        # ###############################another image augmentation##############################
        if image_augmentation(img,albu.GaussianBlur(p=1),dest_image_dir,file_name,'gaussian_blur',0.1):
            GaussianBlur_df=GaussianBlur_df.append(item)
        if image_augmentation(img,albu.GaussianBlur(p=1),dest_image_dir,file_name,'gaussian_blur',0.1):
            GaussianBlur_df=GaussianBlur_df.append(item)
        if image_augmentation(img, albu.IAASharpen(p=1), dest_image_dir, file_name,'sharpen',0.1):
            IAASharpen_df=IAASharpen_df.append(item)
        if image_augmentation(img, albu.HueSaturationValue(hue_shift_limit=172, sat_shift_limit=100, val_shift_limit=100, p=1), dest_image_dir, file_name,'hsv',0.1):
            HueSaturationValue_df=HueSaturationValue_df.append(item)
        if image_augmentation(img, albu.RandomBrightness(limit=0.3, p=1), dest_image_dir, file_name,'bright',0.1):
            RandomBrightness_df=RandomBrightness_df.append(item)
        if image_augmentation(img, albu.RandomContrast(limit=0.8, p=1), dest_image_dir, file_name,'constract',0.1):
            RandomContrast_df=RandomContrast_df.append(item)
        if image_augmentation(img, albu.GaussNoise(p=1),dest_image_dir, file_name,'gaussian_noise',0.1):
            GaussNoise_df=GaussNoise_df.append(item)



    #save new labeled csv

    # if len(perspective_transform_imgurl_list) > 0:
    #     df_perspective_transform = pd.DataFrame({'ImageUrl':perspective_transform_imgurl_list,'Polygon':perspective_transform_polygons_list,'ProductId':perspective_transform_prodcuctid_list})
    #     df_perspective_transform.to_csv(os.path.join(dest_csv_dir,'perspective.csv'),index=False)

    # if len(rotate_90_imgurl_list) > 0:
    #     df_rotate_90 = pd.DataFrame({'ImageUrl':rotate_90_imgurl_list,'Polygon':rotate_90_polygons_list,'ProductId':rotate_90_prodcuctid_list})
    #     df_rotate_90.to_csv(os.path.join(dest_csv_dir,'rotate90.csv'),index=False)

    # if len(rotate_180_imgurl_list) > 0:
    #     df_rotate_180 = pd.DataFrame({'ImageUrl':rotate_180_imgurl_list,'Polygon':rotate_180_polygons_list,'ProductId':rotate_180_prodcuctid_list})
    #     df_rotate_180.to_csv(os.path.join(dest_csv_dir,'rotate180.csv'),index=False)

    # if len(rotate_270_imgurl_list) > 0:
    #     df_rotate_270 = pd.DataFrame({'ImageUrl':rotate_270_imgurl_list,'Polygon':rotate_270_polygons_list,'ProductId':rotate_270_prodcuctid_list})
    #     df_rotate_270.to_csv(os.path.join(dest_csv_dir,'rotate270.csv'),index=False)

    if len(flip_imgurl_list) > 0:
        df_flip_polygons_aug = pd.DataFrame({'ImageUrl': flip_imgurl_list, 'Polygon': flip_polygons_list, 'ProductId': flip_prodcuctid_list})
        df_flip_polygons_aug.to_csv(os.path.join(dest_csv_dir,'mirror_flip.csv'), index=False)

    if len(GaussianBlur_df) > 0:
        GaussianBlur_df['ImageUrl'] = GaussianBlur_df['ImageUrl'].apply(lambda x:x.split('/')[-1]+'_gaussian_blur.jpg')
        GaussianBlur_df.to_csv(os.path.join(dest_csv_dir,'gaussian_blur.csv'), index=False)
    if len(IAASharpen_df) > 0:
        IAASharpen_df['ImageUrl'] = IAASharpen_df['ImageUrl'].apply(lambda x:x.split('/')[-1]+'_sharpen.jpg')
        IAASharpen_df.to_csv(os.path.join(dest_csv_dir,'sharpen.csv'), index=False)
    if len(HueSaturationValue_df) > 0:
        HueSaturationValue_df['ImageUrl'] = HueSaturationValue_df['ImageUrl'].apply(lambda x:x.split('/')[-1]+'_hsv.jpg')
        HueSaturationValue_df.to_csv(os.path.join(dest_csv_dir,'hsv.csv'), index=False)
    if len(RandomBrightness_df) > 0:
        RandomBrightness_df['ImageUrl'] = RandomBrightness_df['ImageUrl'].apply(lambda x:x.split('/')[-1]+'_bright.jpg')
        RandomBrightness_df.to_csv(os.path.join(dest_csv_dir,'bright.csv'), index=False)
    if len(RandomContrast_df) > 0:
        RandomContrast_df['ImageUrl'] = RandomContrast_df['ImageUrl'].apply(lambda x:x.split('/')[-1]+'_constract.jpg')
        RandomContrast_df.to_csv(os.path.join(dest_csv_dir,'constract.csv'), index=False)
    if len(GaussNoise_df) > 0:
        GaussNoise_df['ImageUrl'] = GaussNoise_df['ImageUrl'].apply(lambda x:x.split('/')[-1]+'_gaussian_noise.jpg')
        GaussNoise_df.to_csv(os.path.join(dest_csv_dir,'gaussian_noise.csv'), index=False)

def main(args):
    ABI_TT_image_augmentation(args.csv_dir,args.dest_image_dir,args.dest_csv_dir)

if __name__ == '__main__':
    # tips:修改了之后还需要调试
    main(parser.parse_args())
