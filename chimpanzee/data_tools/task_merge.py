'''
date:2020-11-18
author:shiqing
'''

import os 
import shutil
import argparse 

parser = argparse.ArgumentParser(description="input folder")
parser.add_argument('-f1','--folder1',type=str,required=True)
parser.add_argument('-f2','--folder2',type=str,required=True)
parser.add_argument('-of','--output_folder',type=str,required=True)



def merge_two_folder(folder1,folder2,output_folder):
    '''
    merge two taskid images folder

    Arguments
    ---------
    folder1:
    folder2:
    output_folder:

    Returns:
    --------
    None

    '''

    dirs1 = os.listdir(folder1)
    dirs2 = os.listdir(folder2)
    if not os.path.exists(output_folder):
        os.makedirs(output_folder)
    import pdb;pdb.set_trace()
    for dir in set(dirs1)|set(dirs2):
        subfolder = os.path.join(output_folder,dir)
        if not os.path.exists(subfolder):
            os.makedirs(subfolder)
        subfolder1 = os.path.join(folder1,dir)
        if os.path.exists(subfolder1):
            os.system("cp {} {}".format(subfolder1+'/*',subfolder))
        subfolder2 = os.path.join(folder2,dir)
        if os.path.exists(os.path.join(folder2,dir)):
            os.system("cp {} {}".format(subfolder2+'/*',subfolder))

    print("end")



if __name__ == "__main__":
    args = parser.parse_args()
    merge_two_folder(args.folder1,args.folder2,args.output_folder)
