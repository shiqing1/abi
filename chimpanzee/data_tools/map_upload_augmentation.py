import os
import pandas as pd 
import argparse 
from tqdm import tqdm 

parser = argparse.ArgumentParser()
parser.add_argument('-a','--aug_file',default="/home/zyl/workspace/ABI/group_data/20201231/aug_combined.csv")
parser.add_argument('-u','--upload_file',default="/home/zyl/workspace/ABI/group_data/20201231/upload.csv")
parser.add_argument('-d','--dst_file',default="/home/zyl/workspace/ABI/group_data/20201231/aug_mapped.csv")
args = parser.parse_args()

if __name__ == "__main__":
    df1 = pd.read_csv(args.aug_file)
    df2 = pd.read_csv(args.upload_file)

    delete_list = []
    for i in tqdm(range(df1.shape[0])):
        path = df1.loc[i,'ImageUrl'][:-4]
        url = df2.loc[df2['ImgPath'].str.contains(path),'ImgUrl']
        # df2 = df2.loc[~df2['ImgPath'].str.contains(path)]
        if url.empty:
            delete_list.append(i)
            continue
        df1.loc[i,'ImageUrl']=url.values[0]
    

    df1 = df1.drop(index=delete_list)
    print(df1.shape)
    print(df2.shape)
    df1.to_csv(args.dst_file,index=False)
    df2.to_csv("left_upload.csv",index=False)
