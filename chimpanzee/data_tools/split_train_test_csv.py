'''
author:shiqing
date:2021-01-02
'''

import pandas as pd 
import argparse 
import glob 


'''
split src.csv into train.csv and test.csv
'''

parser = argparse.ArgumentParser()
parser.add_argument('-i','--input',required=True)
parser.add_argument('-tr','--train',required=True)
parser.add_argument('-te','--test',required=True)
args = parser.parse_args()


def main():
    df = pd.read_csv(args.input)
    groups = df.groupby("ProductId")

    test = pd.DataFrame(columns=df.columns)
    train = pd.DataFrame(columns=df.columns)
    few = pd.DataFrame(columns=df.columns)
    for id,group in groups:
        # print(id,group.shape)
        n = group.shape[0]
        if(n>=10):
            test1 = group.iloc[:int(0.1*n),:]
            train1 = group.iloc[int(0.1*n):,:]
            
            if test.empty:
                test = test1
                train = train1
            else:
                test = pd.concat([test,test1])
                train = pd.concat([train,train1])
        else:
            if few.empty:
                few = group
            else:
                few = pd.concat([few,group])

    train = pd.concat([train,few])
    train.to_csv(args.train,index=False)
    test.to_csv(args.test,index=False)

if __name__ == "__main__":
    main()
