'''
date:2021-01-01
author:shiqing
'''

from shiqingTools import  tools 
import argparse 

parser = argparse.ArgumentParser()
parser.add_argument('-f1','--file1',required=True)
parser.add_argument('-f2','--file2',required=True)
parser.add_argument('-o','--output',required=True)
args = parser.parse_args()


if __name__ == "__main__":
    tools.merge_csv(args.output,args.file1,args.file2)

