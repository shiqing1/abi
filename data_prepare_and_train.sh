#!/bin/bash 

#set -e 
#set -x 

if [ $# -ne 2 ];then
	echo "Usage:./data_prepare_and_train.sh mode[group/single] time"
	exit
fi

mode=$1
time=$2

#转成coco格式
trainJson=${time}_train_${mode}_detection.json
if [ ! -f $trainJson ]; then
        python labelme2coco.py /home/haiyan_zeng/data/TrainData/images/${mode}_detection/${time}/train --output $trainJson
fi
testJson=${time}_test_${mode}_detection.json
if [ ! -f $testJson ]; then
        python labelme2coco.py /home/haiyan_zeng/data/TrainData/images/${mode}_detection/${time}/test --output $testJson
fi

#生成tfrecord
tfrecord=/home/haiyan_zeng/data/TrainData/tfrecord/${mode}_detection/${time}/train.record
if [ ! -f $tfrecord ]; then
        python create_coco_tf_record.py --logtostderr     --train_image_dir=/home/haiyan_zeng/data/TrainData/images/${mode}_detection/${time}/train   --test_image_dir=/home/haiyan_zeng/data/TrainData/images/${mode}_detection/${time}/test        --train_annotations_file=${time}_train_${mode}_detection.json  --test_annotations_file=${time}_test_${mode}_detection.json                --include_masks=True --output_dir=/home/haiyan_zeng/data/TrainData/tfrecord/${mode}_detection/${time}
fi


#生成pbtxt
python create_pbtxt.py -i ${time}_train_${mode}_detection.json -o ./train_config/${mode}_detection/labelmap_${time}.pbtxt

#修改mask_rcnn_inception_resnet_v2_1024x1024_coco17_gpu-8.config
python modify_model_config.py -m $mode  -t $time

checkpoint=${time}_${mode}_models
if [ ! -d $checkpoint ]; then
        mkdir -p $checkpoint
fi

export_path=${time}_${mode}_models_inference_graph
if [ ! -d $export_path ]; then
        mkdir -p $export_path
fi

#训练模型
python ~/models/research/object_detection/model_main_tf2.py --pipeline_config_path=/home/haiyan_zeng/code/ABI_TT_model/train_config/${mode}_detection/mask_rcnn_inception_resnet_v2_1024x1024_coco17_gpu-8_${time}.config --model_dir=${time}_${mode}_models  --alsologtostderr

#导出模型
python ~/models/research/object_detection/exporter_main_v2.py  --trained_checkpoint_dir  ${time}_${mode}_models --output_directory ${time}_${mode}_models_inference_graph  --pipeline_config_path train_config/${mode}_detection/mask_rcnn_inception_resnet_v2_1024x1024_coco17_gpu-8_${time}.config
